<?php

return [
    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    // Tambahkan pesan validasi default lainnya di sini...

    'custom' => [
        'email' => [
            'required' => 'Email harus diisi.',
            'email' => 'Format email tidak valid.',
            'unique' => 'Email sudah terdaftar.',
        ],
        'password' => [
            'required' => 'Password harus diisi.',
            'min' => 'Password minimal harus terdiri dari 6 karakter.',
        ],
        'nama_lengkap' => [
            'required' => 'Nama lengkap harus diisi.',
        ],
        'jenis_kelamin' => [
            'required' => 'Jenis kelamin harus diisi.',
        ],
    ],

    'attributes' => [],
];
