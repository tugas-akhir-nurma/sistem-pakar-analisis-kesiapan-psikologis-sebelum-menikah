@extends('user.main')
@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha384-uxLZ81p1VZ8NvdLyHwr7BtAhpq/oNfgSwWg9x3SgqI3g0n2kNq/GjvWSwRnVN+Zr" crossorigin="anonymous">
  <!-- Jquery -->
   <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Hasil Analisis</li> -->
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js"></script>
  <style>
     body, html {
      background-color: #dfe6da; /* Warna margin */
    }
    .content-wrapper {
      background-color: #dfe6da;
      /* Warna pink soft */
      /* Ubah kode warna sesuai preferensi Anda */
    }

    h5 {
      text-align: left;
      margin-bottom: 20px;
      font-size: 2em;
      font-family: 'Times New Roman', Times, serif;
      font-weight: bold;
      margin-left: 20px;
    margin-right: 20px;
    }
    p {
      line-height: 1.6;
      margin-bottom: 20px;
      /* font-size: 1.1em; */
      color: #333;
    }
    .paragraf{
        margin-left: 35px;
        /* font-size: 1.1em; */
    }
    .paragraf2{
        margin-left: 20px;
        /* font-size: 1.1em; */
    }
    .highlight {
      background-color: #eff5ec;
      padding: 10px;
      border-left: 5px solid #000000;
      margin-bottom: 20px;
      text-align: justify;
      font-size: 1.1em;
      margin-left: 30px;
      margin-right: 30px;
      font-family: 'Times New Roman', Times, serif;
    }
    ul {
      padding-left: 20px;
      margin-bottom: 20px;
      /* font-size: 1.1em; */
      text-align: justify;
    }
    .konten{
        margin-left: 50px;
        font-size: 1.1em;
        margin-right: 50px;
        margin-bottom: 70px;
        background-color: #dfe6da;
        font-family: 'Times New Roman', Times, serif;
    }
    li {
      margin-bottom: 10px;
    }
    .btn-read-more {
      display: inline-block;
      padding: 10px 20px;
      margin-top: 20px;
      color: #fff;
      background-color: #dfe6da;
      border: none;
      border-radius: 5px;
      text-align: center;
      text-decoration: none;
      /* font-size: 1em; */
    }
    .btn-read-more:hover {
      background-color: #dfe6da;
    }

    .references {
      /* position: fixed; */
      bottom: 10px;
      right: 10px;
      background-color: #eff5ec;
      padding: 10px;
      border-radius: 5px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); /* Bayangan */
      font-size: 0.9em;
      color: #333;
      font-family: 'Times New Roman', Times, serif;
    }

    
    .references a {
      color: #0066cc; /* Warna link */
      text-decoration: none;
    }
    
    .references a:hover {
      text-decoration: underline; /* Underline saat hover */
    }
  </style>

<h5>Syarat Nikah di KUA yang Harus Diketahui</h5>

  <div class="highlight">
    <p>Menikah adalah tahap kehidupan ketika dua individu telah menemukan pasangan hidup mereka dan berjanji untuk menghabiskan sisa hidup bersama. Persiapan untuk pernikahan merupakan momen yang sangat dinanti-nantikan oleh banyak pasangan. Salah satu tahapan penting yang perlu dilakukan adalah mendaftarkan pernikahan di Kantor Urusan Agama (KUA), biasanya dilakukan paling lambat 3 bulan sebelum pelaksanaan akad. Namun, sebelum melakukan pendaftaran pernikahan di KUA, ada beberapa syarat yang harus dipenuhi dan diketahui. Prosedur ini diatur dalam Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan yang telah mengalami amendemen oleh Undang-Undang Nomor 16 Tahun 2019.</p>
  </div>
  <div style="text-align:center;">
  <img src="{{ asset('/lte/dist/img/KUA3.jpg') }}" alt="Descriptive Image" style="width:430px; height:auto; margin-bottom:10px;">
  <p style="margin-top:1px; font-size:0.6em; color:#555;">Sumber: https://id.pinterest.com/pin/479211216617529729/</p>
</div>

  <div class="konten">
  <ul>
    <strong>1. Syarat Umum</strong>
    <div class="paragraf">
    <li><strong>Fotokopi KTP : </strong> Kedua calon pengantin harus menyediakan fotokopi KTP yang masih berlaku.</li>
    </div>
    <div class="paragraf">
    <li><strong>Fotokopi Kartu Keluarga(KK) : </strong> Kedua calon pengantin harus menyediakan fotokopi KK masing-masing.</li>
    </div>
    <div class="paragraf">
    <li><strong>Akta Kelahiran : </strong> Fotokopi akta kelahiran dari kedua calon pengantin.</li>
    </div>
    <div class="paragraf">
    <li><strong>Pas Foto : </strong> Pas foto ukuran 2x3 dan 3x4 dengan latar belakang biru sebanyak 4 lembar atau lebih masing-masing ukuran.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Pengantar RT/RW : </strong> Surat pengantar dari RT/RW setempat yang menyatakan bahwa calon pengantin adalah warga yang berdomisili di wilayah tersebut.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Keterangan Belum Menikah : </strong> Surat yang menyatakan bahwa calon pengantin belum pernah menikah, bisa didapatkan dari kelurahan setempat.</li>
    </div>
    <div class="paragraf">
    <li>Surat keterangan untuk nikah (model N1) Surat keterangan asal-usul (model N2) Surat persetujuan mempelai (model N3) Surat keterangan tentang orang tua (model N4) yang didapatkan di kelurahan.</li>
    </div>
    <div class="paragraf">
    <li>Surat pemberitahuan kehendak nikah (model N7) apabila calon pengantin berhalangan, pemberitahuan nikah dapat dilakukan oleh wali atau wakilnya.</li>
    </div>
    <div class="paragraf">
    <li>Bukti imunisasi TT1 calon pengantin wanita, Kartu imunisasi, dan Imunisasi TT II dari Puskesmas setempat.</li>
    </div> 
    <strong>2. Persyaratan Nikah untuk Pria</strong>
    <div class="paragraf">
    <li><strong>Surat Izin dari Atasan : </strong> Jika calon pengantin pria adalah anggota TNI/Polri, surat izin dari atasan diperlukan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Izin Orang Tua/Wali : </strong> Jika calon pengantin pria belum berusia 21 tahun, surat izin dari orang tua atau wali diperlukan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Keterangan Kematian Istri (jika duda) : </strong> Jika calon pengantin pria adalah duda, surat keterangan kematian istri sebelumnya harus dilampirkan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Cerai : </strong> Jika calon pengantin pria pernah bercerai, surat cerai harus dilampirkan sebagai bukti bahwa pernikahan sebelumnya sudah sah bercerai.</li>
    </div>
    <div class="paragraf">
    <li>Surat keterangan untuk nikah (model N1) Surat keterangan asal-usul (model N2) Surat persetujuan mempelai (model N3) Surat keterangan tentang orang tua (model N4) yang didapatkan di kelurahan.</li>
    </div>
    <div class="paragraf">
    <li>Surat pemberitahuan kehendak nikah (model N7) apabila calon pengantin berhalangan, pemberitahuan nikah dapat dilakukan oleh wali atau wakilnya.</li>
    </div>
    <div class="paragraf">
    <li>Bukti imunisasi TT1 calon pengantin wanita, Kartu imunisasi, dan Imunisasi TT II dari Puskesmas setempat.</li>
    </div>
    <div class="paragraf">
    <li>Pas foto kedua calon pengantin dengan ukuran 2x3 dan 3x4 dengan latar belakang biru sebanyak 4 lembar atau lebih masing-masing ukuran.</li>
    </div>
    <strong>3. Persyaratan Nikah untuk Wanita</strong>
    <div class="paragraf">
    <li><strong>Surat Izin dari Atasan : </strong> Jika calon pengantin wanita adalah anggota TNI/Polri, surat izin dari atasan diperlukan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Izin Orang Tua/Wali : </strong> Jika calon pengantin wanita belum berusia 21 tahun, surat izin dari orang tua atau wali diperlukan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Keterangan Kematian Suami (jika janda) : </strong> Jika calon pengantin wanita adalah janda, surat keterangan kematian suami sebelumnya harus dilampirkan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Cerai : </strong> Jika calon pengantin wanita pernah bercerai, surat cerai harus dilampirkan sebagai bukti bahwa pernikahan sebelumnya sudah sah bercerai.</li>
    </div>
    <div class="paragraf">
    <li>Surat keterangan untuk nikah (model N1) Surat keterangan asal-usul (model N2) Surat persetujuan mempelai (model N3) Surat keterangan tentang orang tua (model N4) yang didapatkan di kelurahan.</li>
    </div>
    <div class="paragraf">
    <li>Surat pemberitahuan kehendak nikah (model N7) apabila calon pengantin berhalangan, pemberitahuan nikah dapat dilakukan oleh wali atau wakilnya.</li>
    </div>
    <div class="paragraf">
    <li>Bukti imunisasi TT1 calon pengantin wanita, Kartu imunisasi, dan Imunisasi TT II dari Puskesmas setempat.</li>
    </div>
    <div class="paragraf">
    <li>Pas foto kedua calon pengantin dengan ukuran 2x3 dan 3x4 dengan latar belakang biru sebanyak 4 lembar atau lebih masing-masing ukuran.</li>
    </div>
    <strong>4. Persyaratan Bagi Orang Tua/Wali Calon Pengantin</strong>
    <div class="paragraf">
    <li><strong>Fotokopi KTP Orang Tua/Wali : </strong> Fotokopi KTP orang tua atau wali dari kedua calon pengantin.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Izin Orang Tua/Wali : </strong> Surat izin yang menyatakan persetujuan orang tua atau wali atas pernikahan tersebut, terutama jika calon pengantin belum mencapai usia 21 tahun.</li>
    </div>
    <div class="paragraf">
    <li><strong>Surat Keterangan Wali Hakim : </strong> Jika calon pengantin wanita tidak memiliki wali nasab, maka diperlukan surat keterangan wali hakim dari KUA setempat.</li>
    </div>
  </ul>
  </div>
  
  <h5> Cara Mendaftar Pernikahan di KUA</h5>
  <div class="highlight">
    <p>Pendaftaran pernikahan di KUA dapat dilakukan dengan dua metode, yaitu secara online melalui situs Simkah dan secara offline dengan datang langsung ke KUA. Berikut adalah langkah-langkahnya  :</p>
  </div>
  <div style="text-align:center;">
  <img src="{{ asset('/lte/dist/img/KUA4.jpg') }}" alt="Descriptive Image" style="width:430px; height:auto; margin-bottom:10px;">
  <p style="margin-top:1px; font-size:0.6em; color:#555;">Sumber: https://id.pinterest.com/pin/13651605113511685/</p>
</div>
  <div class="konten">
  <ul>
  <strong>1. Mendaftar Pernikahan di KUA Secara Online</strong>
  <div class="paragraf">
    <li><strong>Login ke Akun Simkah : </strong> Akses situs Simkah (simkah.kemenag.go.id) dan login ke akun Anda. Jika belum memiliki akun, Anda perlu mendaftar terlebih dahulu.</li>
    </div>
    <div class="paragraf">
    <li><strong>Daftar Nikah : </strong> Setelah login, pilih menu "Daftar Nikah" untuk memulai proses pendaftaran.</li>
    </div>
    <div class="paragraf">
    <li><strong>Nomor Daftar Nikah dan Rekomendasi Nikah : </strong> Setelah mengisi formulir pendaftaran, Anda akan mendapatkan nomor daftar nikah dan rekomendasi nikah.</li>
    </div>
    <div class="paragraf">
    <li><strong>Pilih Tempat dan Waktu Pelaksanaan Nikah : </strong> Tentukan tempat dan waktu pelaksanaan akad nikah sesuai dengan keinginan Anda.</li>
    </div>
    <div class="paragraf">
    <li><strong>Masukkan Data Calon Suami dan Calon Istri : </strong> Isi data lengkap mengenai calon suami dan calon istri.</li>
    </div>
    <div class="paragraf">
    <li><strong>Unggah dan Lengkapi Dokumen yang Diminta : </strong> Unggah dokumen-dokumen yang diminta seperti KTP, KK, akta kelahiran, dan lain-lain.</li>
    </div>
    <div class="paragraf">
    <li><strong>Nomor Telepon dan Alamat Email : </strong> Pastikan nomor telepon dan alamat email yang dimasukkan aktif untuk memudahkan komunikasi.</li>
    </div>
    <div class="paragraf">
    <li><strong>Unggah Foto : </strong> Unggah pas foto calon pengantin dengan ketentuan yang sudah ditetapkan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Cetak Bukti Pendaftaran Nikah : </strong> Setelah semua data terisi dan dokumen terunggah, cetak bukti pendaftaran nikah untuk dibawa ke KUA saat verifikasi.</li>
    </div>
    <div class="paragraf">
    <li>Hadir di tanggal yang telah dipilih bersama calon pasangan dan wali nikah. Bawa seluruh bukti pendaftaran dan dokumen pernikahan asli beserta saksi nikah minimal 2 orang.</li>
    </div>
    <strong>2. Mendaftar Pernikahan di KUA Secara Offline</strong>
    <div class="paragraf">
    <li><strong>Datangi Kantor Kelurahan : </strong>  Dengan membawa surat pengantar nikah dari RT/RW dan data diri Anda dan calon pasangan, lengkapi dan serahkan formulir permohonan nikah (N1 s.d. N4) di kelurahan tempat tinggal calon pengantin.</li>
    </div>
    <div class="paragraf">
    <li><strong>Serahkan Dokumen ke KUA : </strong>  Setelah mendapatkan formulir lengkap dan ditandatangani oleh pihak kelurahan, bawa seluruh dokumen yang telah disiapkan ke KUA kecamatan tempat akan dilaksanakannya akad nikah. Anda perlu datang ke KUA minimal 10 hari sebelum dilaksanakannya akad nikah.</li>
    </div>
    <div class="paragraf">
    <li><strong>Pemeriksaan Data : </strong>  Petugas KUA akan memeriksa kelengkapan dan keabsahan dokumen yang diserahkan.</li>
    </div>
    <div class="paragraf">
    <li><strong>Penentuan Tanggal Nikah : </strong>  Diskusikan dengan petugas KUA dan penghulu mengenai jadwal yang tersedia untuk pelaksanaan akad nikah.</li>
    </div>
    <div class="paragraf">
    <li>Mengikuti bimbingan pra nikah yang dilakukan oleh penyuluh agama dari KUA.</li>
    </div>
  </ul>
  </div>

  <h5>Alur Prosesi Nikah di KUA</h5>
  <div class="highlight">
    <p>Setelah mendaftarkan pernikahan, terdapat beberapa langkah yang harus diikuti dalam proses pernikahan di KUA. Berikut adalah alur prosesi pernikahan di KUA  :</p>
  </div>
  <div style="text-align:center;">
  <img src="{{ asset('/lte/dist/img/KUA5.jpg') }}" alt="Descriptive Image" style="width:430px; height:auto; margin-bottom:10px;">
  <p style="margin-top:1px; font-size:0.6em; color:#555;">Sumber: https://id.pinterest.com/pin/127789708169223943/</p>
</div>
  <div class="konten">
  <ul>
    <strong>1. Mendatangi Ketua RT</strong>
    <div class="paragraf2">
    <P>Langkah pertama yang harus dilakukan adalah mendatangi ketua RT setempat untuk mendapatkan surat pengantar. Surat pengantar ini diperlukan untuk mengurus surat keterangan belum menikah dari kelurahan.</p>
    </div>
    <strong>2. Mendatangi Kelurahan/Desa</strong> 
    <div class="paragraf2">
    <P>Setelah mendapatkan surat pengantar dari ketua RT, langkah selanjutnya adalah mendatangi kelurahan atau desa untuk mengurus surat keterangan belum menikah dan surat pengantar ke KUA. Pastikan membawa dokumen-dokumen yang diperlukan seperti KTP, KK, dan akta kelahiran. Anda juga akan mendapatkan Surat keterangan untuk nikah (model N1), Surat keterangan asal-usul (model N2), Surat persetujuan mempelai (model N3), dan Surat keterangan tentang orang tua (model N4).</p>
    </div>
    <strong>3. Keterangan Dispensasi Kecamatan</strong> 
    <div class="paragraf2">
    <P>Jika ada calon pengantin yang belum mencapai usia minimal pernikahan atau pernikahan dilaksanakan kurang dari 10 hari, calon pengantin perlu mendapatkan keterangan dispensasi dari kecamatan. Dispensasi ini diberikan oleh camat setempat.</p>
    </div>
    <strong>4. Mendaftar Secara Offline atau Online</strong> 
    <div class="paragraf2">
    <P>Setelah seluruh persyaratan nikah lengkap, Anda dapat mendaftar untuk melakukan akad nikah secara offline atau online. Jika Anda mendaftar secara online, kunjungi situs Simkah dan ikuti prosedur yang tertera.</p>
    </div>
    <strong>5. Mendatangi KUA Tempat Akad Dilaksanakan</strong> 
    <div class="paragraf2">
    <P>Langkah terakhir adalah mendatangi KUA tempat akad nikah akan dilaksanakan untuk menyerahkan semua dokumen yang sudah dilengkapi dan melakukan verifikasi akhir. Pastikan semua syarat nikah di KUA sudah terpenuhi dan dokumen sudah lengkap untuk menghindari masalah pada hari pernikahan.</p>
    </div>
    <strong>6. Membayar Biaya Akad Nikah</strong> 
    <div class="paragraf2">
    <P>Pembayaran biaya akad nikah dilakukan di KUA. Biaya ini berbeda-beda tergantung pada tempat dan waktu pelaksanaan akad nikah. Jika akad nikah dilakukan di kantor KUA pada jam kerja, maka biaya yang dikenakan umumnya tidak dipungut alias gratis. Namun, jika akad nikah dilakukan di luar kantor KUA atau di luar jam kerja KUA, seperti di rumah, gedung, atau tempat lain yang telah disepakati, maka calon pengantin harus membayar biaya administrasi. Besaran biaya tambahan ini adalah Rp 600.000 sesuai dengan ketentuan yang berlaku di daerah masing-masing. Biaya ini mencakup transportasi dan waktu tambahan petugas KUA yang akan memimpin prosesi pernikahan di lokasi yang dipilih. Untuk memastikan tidak ada kendala dalam proses pembayaran, calon pengantin disarankan untuk mengkonfirmasi langsung ke KUA setempat mengenai rincian biaya yang harus dipersiapkan.</p>
    </div>
    <strong>7. Mengikuti Bimbingan Pra-Nikah</strong> 
    <div class="paragraf2">
    <P>Setelah seluruh dokumen persyaratan terverifikasi, Anda akan mendapatkan bimbingan pra nikah yang meliputi pengetahuan mengenai pernikahan dalam islam, hak dan kewajiban sebagai suami dan istri, dan bagaimana membangun rumah tangga yang harmonis. Calon pengantin yang menghadiri sidang nikah akan diberi pertanyaan seputar urusan agama seperti membaca Al Qur’an atau mengaji, bacaan sholat dan pertanyaan lainnya tentang pernikahan. Mengapa pertanyaan ini penting? Tentunya karena sholat adalah tiang agama dan suami juga akan menjadi pemimpin dalam rumah tangga maka otomatis harus mengerti bacaan solat serta mengaji. Di dalam sidang atau rapak nikah, petugas KUA juga akan memberi edukasi tentang pernikahan untuk kedua calon pengantin. Edukasi tersebut terdiri dari bagaimana cara yang benar dalam berumah tangga, cara mengontrol emosi, perlakuan istri terhadap suami agar bisa mencapai keluarga yang sakinah mawadah wa rahmah. Pemberian edukasi kepada calon pengantin bertujuan untuk menghindari perceraian dan masalah rumah tangga yang biasanya terjadi.</p>
    </div>
    <strong>8. Melakukan Akad Nikah</strong> 
    <div class="paragraf2">
    <P>Langkah terakhir adalah melakukan akad nikah pada tanggal yang telah dipilih sebelumnya. Akad nikah akan dipimpin oleh Pejabat Pencatat Nikah dan wajib dihadiri oleh wali nikah beserta saksi nikah minimal 2 orang. Anda juga dapat mengundang keluarga dan sahabat calon pengantin pria atau wanita, tokoh agama setempat, namun hal tersebut tidak bersifat wajib.</p>
    </div>
  </ul>
  </div>

  <div class="references">
    <p><strong>Sumber : </strong></p>
    <ul>
    <li><a href="https://www.bfi.co.id/id/blog/dear-catin-simak-syarat-nikah-di-kua-yang-harus-diketahui" target="_blank">Dear Catin: Simak Syarat Nikah di KUA yang Harus Diketahui</a></li>
    <li><a href="https://heikamu.com/ini-pertanyaan-pra-nikah-di-kua-yang-harus-kamu-tahu/" target="_blank">Ini Pertanyaan Pra Nikah di KUA yang Harus Kamu Tahu</a></li>
    </ul>
  </div>
</div>

@endsection
