@extends('USER.main')

@section('content')


<!DOCTYPE html>





<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- <h1 class="m-0">Form Analisis</h1> -->
                </div><!-- /.col -->
                
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="card"> -->
                        <!-- <div class="card-header"> -->
                            <!-- <h3 class="card-title">Pilih Jawaban</h3> -->
                        <!-- </div> -->
    <style>
    .table {
    width: 100%; /* Sesuaikan lebar tabel sesuai kebutuhan */
    text-align: center;
    }
    .card-body {
    padding: 20px; /* Tambahkan padding di dalam card body */
    margin-bottom: 20px; /* Tambahkan margin di bawah card body */
}
.card-body {
    background-color:  #eff5ec; /* Ubah warna latar belakang card body */
    padding: 10px; /* Tambahkan padding di dalam card body */
    border-radius: 10px; /* Agar card body memiliki sudut yang lebih melengkung */
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1); /* Tambahkan bayangan pada card body */
    max-width: 800px; /* Atur lebar maksimum card body */
    margin: auto; /* Atur margin auto untuk posisi tengah */
margin-top: 80px;
font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
color: black; /* Warna teks menjadi hitam */
}

.card-body {
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1); /* Tambahkan bayangan pada card body */
    border-radius: 10px; /* Agar card body memiliki sudut yang lebih melengkung */
    font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
    color: black; /* Warna teks menjadi hitam */
}

    .question-column {
      white-space: pre-line;
      word-wrap: break-word;
      max-width: 180px; 
      text-align: left;
      font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
      color: black; /* Warna teks menjadi hitam */
    }
    .pilihan-column {
      text-align: left;
      max-width: 30px; 
      font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
      color: black; /* Warna teks menjadi hitam */
    }

    .content-wrapper {
        background-color: #dfe6da; 
    }

    .controls {
    display: flex;
    justify-content: left;  /* Menempatkan isi flexbox di pojok kanan */
    margin-top: 10px; /* Jarak antara tombol dengan card-body */
    padding-right: 100px;
    padding-LEFT: 100px;
}

.controls button {
    margin-right: 10px; /* Memberi ruang di sebelah kanan tombol */
    background-color: black; /* Ubah warna latar belakang tombol */
    color: white; /* Ubah warna teks tombol */
    border: none; 
    font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
}

.nomor{
    display: flex;
    justify-content: flex-end; 
    margin-bottom: 5px; 
    font-weight: bold;
}
.soal{
    margin-top: 5px; 
}

    </style>
                      <div class="card-body">
    @php
        // Filter pertanyaan yang sesuai dengan gender "Perempuan" atau "Umum"
        $filteredPertanyaan = $pertanyaan->filter(function ($p) {
            return $p->gender == 'Perempuan' || $p->gender == 'Umum';
        });
        $totalPertanyaan = $filteredPertanyaan->count();
        $index = 0;
    @endphp

    <form id="quizForm" action="{{ route('user.storepsikologis') }}" method="POST">
        @csrf
        <div class="slider">
            @foreach ($filteredPertanyaan as $p)
                <div class="slide">
                    <p class="nomor">{{ ++$index }} / {{ $totalPertanyaan }}</p>
                    <h5 class="soal">{{ $p->kesiapan }}</h5>
                    <div class="pilihan">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="{{ $p->id }}" id="jawaban_{{ $loop->iteration }}_1" value="{{ $p->tidak }}|{{ $p->CFH }}|tidak" step="any">
                            <label class="form-check-label" for="jawaban_{{ $loop->iteration }}_1">
                                Tidak
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="{{ $p->id }}" id="jawaban_{{ $loop->iteration }}_2" value="{{ $p->tidak_tahu }}|{{ $p->CFH }}|tidaktahu" step="any">
                            <label class="form-check-label" for="jawaban_{{ $loop->iteration }}_2">
                                Tidak Tahu
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="{{ $p->id }}" id="jawaban_{{ $loop->iteration }}_2" value="{{ $p->sedikit_setuju }}|{{ $p->CFH }}|sedikitsetuju" step="any">
                            <label class="form-check-label" for="jawaban_{{ $loop->iteration }}_2">
                                Sedikit Setuju
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="{{ $p->id }}" id="jawaban_{{ $loop->iteration }}_2" value="{{ $p->cukup_setuju }}|{{ $p->CFH }}|cukupsetuju" step="any">
                            <label class="form-check-label" for="jawaban_{{ $loop->iteration }}_2">
                                Cukup Setuju
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="{{ $p->id }}" id="jawaban_{{ $loop->iteration }}_2" value="{{ $p->setuju }}|{{ $p->CFH }}|setuju" step="any">
                            <label class="form-check-label" for="jawaban_{{ $loop->iteration }}_2">
                                Setuju
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="{{ $p->id }}" id="jawaban_{{ $loop->iteration }}_2" value="{{ $p->sangat_setuju }}|{{ $p->CFH }}|sangatsetuju" step="any">
                            <label class="form-check-label" for="jawaban_{{ $loop->iteration }}_2">
                                Sangat Setuju
                            </label>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- <div class="controls"> -->
        </div>
    </form>
</div>
</div>

<div class="controls">
    <button class="btn btn-primary" id="prevBtn" onclick="prevSlide()">PREV</button>
    <button class="btn btn-primary" id="nextBtn" onclick="nextSlide()">NEXT</button>
    <button type="button" class="btn btn-primary" id="submitBtn" onclick="submitForm()">SUBMIT</button>
</div>
</form>
</div>

<script>
    var currentSlide = 0;
    showSlide(currentSlide);

    function showSlide(n) {
        var slides = document.getElementsByClassName("slide");
        for (var i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slides[n].style.display = "block";
        if (n === 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n === slides.length - 1) {
            document.getElementById("nextBtn").style.display = "none";
            document.getElementById("submitBtn").style.display = "inline";
        } else {
            document.getElementById("nextBtn").style.display = "inline";
            document.getElementById("submitBtn").style.display = "none";
        }
    }

    function prevSlide() {
        currentSlide--;
        showSlide(currentSlide);
    }

    function nextSlide() {
        if (validateSlide(currentSlide)) {
            currentSlide++;
            showSlide(currentSlide);
        } else {
            alert("Silakan pilih jawaban sebelum melanjutkan.");
        }
    }

    function validateSlide(n) {
        var slide = document.getElementsByClassName("slide")[n];
        var inputs = slide.querySelectorAll("input[type='radio'], input[type='checkbox']");

        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked) {
                return true;
            }
        }
        return false;
    }

    function submitForm() {
        if (validateSlide(currentSlide)) {
            var form = document.getElementById("quizForm");
            form.action = "{{ route('user.storepsikologis') }}";
            form.submit();
        } else {
            alert("Silakan pilih jawaban sebelum mengirimkan formulir.");
        }
    }
</script>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
