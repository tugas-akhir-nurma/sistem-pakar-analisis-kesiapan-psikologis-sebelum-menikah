@extends('user.main')

@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah User</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <style>
    .h2-styled {
        font-family: 'Roboto', sans-serif; /* Ubah jenis font menjadi Roboto */
        font-weight: 300; /* Gunakan jenis font ringan */
        font-size: 24px; /* Atur ukuran font */
        color: #666; /* Ubah warna teks menjadi abu-abu lebih terang */
        text-align: center; /* Pusatkan teks */
        margin-bottom: 20px; /* Beri margin bawah */
    }
    /*  */

    .content-wrapper {
        background-color: #dfe6da; /* Warna pink soft */ /* Ubah kode warna sesuai preferensi Anda */
    /* color: white; Warna teks yang kontras dengan latar belakang */
    }
    .card.card-primary {
    background-color:  #eff5ec; /* Mengatur latar belakang kartu menjadi transparan dengan opasitas */
    border-radius: 10px; /* Memberikan sudut yang lebih bulat pada kartu */
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1); /* Menambahkan bayangan pada kartu */
    transition: all 0.3s ease; /* Efek transisi saat hover */
    /* margin-top: 50px; */
    border: 1px solid #000000; /* Ganti #ccc dengan warna border yang Anda inginkan */
    margin-top: 70px;
}

.card.card-primary:hover {
    transform: translateY(-3px); /* Mengangkat kartu sedikit saat dihover */
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.15); /* Meningkatkan bayangan saat dihover */
}

.card.card-primary .card-body {
    background-color: rgba(255, 255, 255, 0.1); /* Mengatur latar belakang isi kartu menjadi transparan dengan opasitas */
    border-radius: 10px; /* Memberikan sudut yang lebih bulat pada isi kartu */
}

.card.card-primary .form-group label {
    color: black; /* Mengatur warna teks label menjadi hitam */
    font-family: 'Times New Roman', Times, serif;  /* Mengatur model font label */
}

.card.card-primary .form-control {
    background-color: rgba(255, 255, 255, 0.1); /* Mengatur latar belakang input menjadi transparan dengan opasitas */
    border-color: black; /* Mengatur warna garis input menjadi hitam */
    color: black; /* Mengatur warna teks input menjadi hitam */
    font-family: Arial, sans-serif; /* Mengatur model font input */
}

.card.card-primary .btn-primary {
    background-color:  black; /* Mengatur latar belakang tombol menjadi transparan */
    border-color: black; /* Mengatur warna garis tombol menjadi hitam */
    color: white; /* Mengatur warna teks tombol menjadi hitam */
    transition: background-color 0.3s ease, color 0.3s ease; /* Efek transisi saat hover */
}

.card.card-primary .btn-primary:hover {
    background-color: black; /* Mengatur latar belakang tombol menjadi hitam saat dihover */
    color: white; /* Mengatur warna teks tombol saat dihover */
}

.title {
        font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
        color: black; /* Warna teks menjadi hitam */
        margin-bottom: 15px; /* Jarak bawah antara judul */
        margin-top: 0px; /* Mengurangi margin atas untuk mendekatkan dengan logo */
        text-align: center;
        font-weight: bold;
        padding-left: 50px;
        padding-right: 50px;
      }

    .title2 {
    font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
    color: black; /* Warna teks menjadi hitam */
    /* margin-bottom: 30px; Jarak bawah antara judul */
    margin-top: 5px;
    text-align: left;
    /* line-height: 1.6; */
    display: inline-block;
    /* font-weight: bold; */
    }

    .text-center{
      margin-top: 20px;
      font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
    color: black; 
    font-weight: bold;
    }

</style>


    <section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
            <!-- <h2 class="h2-styled">Isi nama lengkap dan jenis kelamin untuk memulai analisis</h2> -->
                <div class="card card-primary">
                    <!-- <div class="card-header">
                        <h3 class="card-title">Form Tambah Data</h3>
                    </div> -->
                    <div class="card-body">
                        <form action="{{ route('user.storeanalisis') }}" method="POST">
                            @csrf
                            <h4 class="title">IDENTITAS ANALIS</h4>
                            <table>
            <tbody>
           <tr><td><h5 class="title2">Nama Lengkap</h5></td><td><h5 class="title2"> : </h5></td><td><h5 class="title2" id="nama_lengkap" name="nama_lengkap"> {{ $namalengkap }}</h5></td></tr>
            <tr><td><h5 class="title2">Jenis Kelamin</h5></td><td><h5 class="title2"> : </h5></td><td><h5 class="title2" id="jenis_kelamin" name="jenis_kelamin"> {{ $jeniskelamin }}</h5></td></tr>
            </tbody></table>
                            
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">MULAI</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

  </div>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if($message = Session::get('success'))
<script> 
Swal.fire({
  position: "center-center",
  icon: "success",
  title: "{{ $message }}",
  showConfirmButton: false,
  timer: 2500
});
</script>                                  
@endif
@endsection