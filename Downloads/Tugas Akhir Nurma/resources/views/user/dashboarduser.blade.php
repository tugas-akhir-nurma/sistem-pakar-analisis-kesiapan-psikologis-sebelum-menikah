@extends('user.main')

@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap">

  <!-- Content Header (Page header) -->
  <div class="content-header">
   
  </div>
  <!-- /.content-header -->
<section class="content text-center">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 text-justify">
        <!-- <div class="card"> -->
        <!-- <div class="col-sm-12 text-center"> -->
            <h4 class="col-sm-12 text-center">GRAFIK RIWAYAT ANALISIS</h4>
          </div>
          <div class="card-body">
            <div style="width: 90%; margin: 0 auto;">
                @if (isset($analisis) && count($analisis) > 0)
                  <canvas id="myChart"></canvas>
                  <div class="analysis-details">
    <h4 class="text-center">Detail Riwayat Analisis</h4>
    </div>
    <table class="table">
    <thead>
        <tr>
            <td>Tanggal Analisis</td>
            <td>Nilai Akhir</td>
            <td>Tingkat Kesiapan</td>
            <td>Detail Hasil Analisis</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($analisis as $a)
            @php
                $kesiapan = '';
                if ($a->nilai_akhir < 45) {
                    $kesiapan = 'Rendah';
                } elseif ($a->nilai_akhir >= 45 && $a->nilai_akhir < 75) {
                    $kesiapan = 'Sedang';
                } else {
                    $kesiapan = 'Tinggi';
                }

                // Filter results berdasarkan tanggal_analisis yang sesuai
                $filteredResults = $results->filter(function($r) use ($a) {
                    return \Carbon\Carbon::parse($r->riwayat_user_id) == \Carbon\Carbon::parse($a->id);
                });
            @endphp
            <tr>
                <td>{{ \Carbon\Carbon::parse($a->tanggal_analisis)->format('d/m/Y') }}</td>
                <td>{{ $a->nilai_akhir }}%</td>
                <td>{{ $kesiapan }}</td>
                <td>
                    @if ($filteredResults->isEmpty())
                        Tidak ada detail hasil analisis
                    @else
                        @foreach ($filteredResults as $r)
                            {{ $r->faktor }} : Anda mendapatkan skor <strong>{{ $r->total_cf}}</strong> dari total maksimal skor <strong>{{ $r->cfmaksimal }}</strong><br>
                        @endforeach
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

</div>



                @else
                  <div class="text-center">
                    <i class="fas fa-chart-line fa-5x" style="color: black;"></i>
                      <h5 class="text-center">Anda belum mempunyai riwayat analisis, lakukan analisis sekarang.</h5>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- </div> -->

@if (isset($analisis) && count($analisis) > 0)
<!-- Pastikan ini di bagian akhir halaman atau gunakan defer pada script -->
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var ctx = document.getElementById('myChart').getContext('2d');

    var labels = [
      @foreach ($analisis as $a)
        '{{ \Carbon\Carbon::parse($a->tanggal_analisis)->format('d/m/Y') }}',
      @endforeach
    ];

    var data = [
      @foreach ($analisis as $a)
        '{{ $a->nilai_akhir }}',
      @endforeach
    ].map(value => parseFloat(value)); // Convert to float for proper charting

    var chartType = data.length === 1 ? 'bar' : 'line';

    var gradient = ctx.createLinearGradient(0, 0, 0, 400);
    gradient.addColorStop(0, 'rgba(75, 192, 192, 0.2)');
    gradient.addColorStop(1, 'rgba(153, 102, 255, 0.2)');

    var chart = new Chart(ctx, {
      type: chartType,
      data: {
        labels: labels,
        datasets: [{
          label: 'Nilai Akhir',
          data: data,
          backgroundColor: gradient,
          borderColor: 'rgba(75, 192, 192, 1)',
          borderWidth: 2,
          pointBackgroundColor: 'rgba(75, 192, 192, 1)',
          pointBorderColor: 'rgba(255, 255, 255, 1)',
          pointBorderWidth: 2,
          pointRadius: 5,
          fill: true
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
            grid: {
              color: 'rgba(200, 200, 200, 0.2)'
            },
            ticks: {
              color: '#333',
              font: {
                family: 'Roboto',
                size: 14
              },
              callback: function(value) {
                return value + '%'; // Add '%' to y-axis labels
              }
            }
          },
          x: {
            grid: {
              color: 'rgba(200, 200, 200, 0.2)'
            },
            ticks: {
              color: '#333',
              font: {
                family: 'Roboto',
                size: 14
              }
            }
          }
        },
        plugins: {
          tooltip: {
            callbacks: {
              label: function(context) {
                return context.parsed.y + '%'; // Add '%' to tooltip labels
              }
            },
            backgroundColor: 'rgba(0, 0, 0, 0.7)',
            titleFont: {
              family: 'Roboto',
              size: 16
            },
            bodyFont: {
              family: 'Roboto',
              size: 14
            }
          }
        },
        elements: {
          line: {
            tension: 0.4
          }
        },
        responsive: true,
        maintainAspectRatio: false
      }
    });
  });
</script>
@endif
<style>
  #chartContainer {
  display: flex;
  justify-content: center;
  align-items: center; /* Jika ingin tengah secara vertikal juga */
}

#myChart {
  width: 900px !important;
  height: 700px !important;
  padding-right: 1% !important;
  display: flex;
  justify-content: center;
  align-items: center;
}

  .content-wrapper {
      background-color: #dfe6da;
      /* Warna pink soft */
      /* Ubah kode warna sesuai preferensi Anda */
    }
  
  .card {
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    border-radius: 8px;
  }
  
  .col-sm-12.text-center {
  font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
  color: black; 
  font-weight: bold;
  /* margin-right:50%; */
}

.text-center {
  font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
  color: black; 
  /* font-weight: bold; */
  /* margin-top:20px; */
}

.analysis-details{
  margin-top:80px;
}

.lead{
  font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
  color: black; 
}

.card-header {
    background-color:  #dfe6da;
    color: black;
    text-align: center; /* Menambahkan properti untuk menengahkan teks */
  }
  .lead {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    font-size: 1.25rem;
    color: #6c757d;
  }
  #analysis-details {
    margin-top: 20px;
  }
  #analysis-details h5 {
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
    color: #333;
  }
  #analysis-details ul {
    list-style-type: none;
    padding: 0;
  }
  #analysis-details ul li {
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    color: #555;
    margin-bottom: 5px;
  }
  .d-flex {
    display: flex !important;
  }
  .justify-content-center {
    justify-content: center !important;
  }
 
  .table {
        width: 100%;
        border-collapse: collapse;
        font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
        color: black; 
        font-size: 14px;
    }

    .table td {
        border: 1px solid black;
        padding: 8px;
        text-align: center;
        font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
        color: black; 
    }

    .table td {
        background-color: #DCDCDC;
    }
</style>

@endsection
