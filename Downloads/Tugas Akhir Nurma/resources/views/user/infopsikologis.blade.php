@extends('user.main')
@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha384-uxLZ81p1VZ8NvdLyHwr7BtAhpq/oNfgSwWg9x3SgqI3g0n2kNq/GjvWSwRnVN+Zr" crossorigin="anonymous">
  <!-- Jquery -->
   <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Hasil Analisis</li> -->
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js"></script>
  <style>
     body, html {
      background-color: #dfe6da; /* Warna margin */
    }
    .content-wrapper {
      background-color: #dfe6da;
      /* Warna pink soft */
      /* Ubah kode warna sesuai preferensi Anda */
    }

    h5 {
      text-align: left;
      margin-bottom: 20px;
      font-size: 2em;
      font-family: 'Times New Roman', Times, serif;
      font-weight: bold;
      margin-left: 20px;
    margin-right: 20px;
    }
    p {
      line-height: 1.6;
      margin-bottom: 20px;
      /* font-size: 1.1em; */
      color: #333;
    }
    .paragraf{
        margin-left: 20px;
        /* font-size: 1.1em; */
    }
    .highlight {
      background-color: #eff5ec;
      padding: 10px;
      border-left: 5px solid #000000;
      margin-bottom: 20px;
      text-align: justify;
      font-size: 1.1em;
      margin-left: 30px;
      margin-right: 30px;
      font-family: 'Times New Roman', Times, serif;
    }
    ul {
      padding-left: 20px;
      margin-bottom: 20px;
      /* font-size: 1.1em; */
      text-align: justify;
    }
    .konten{
        margin-left: 50px;
        font-size: 1.1em;
        margin-right: 50px;
        margin-bottom: 70px;
        background-color: #dfe6da;
        font-family: 'Times New Roman', Times, serif;
    }
    li {
      margin-bottom: 10px;
    }
    .btn-read-more {
      display: inline-block;
      padding: 10px 20px;
      margin-top: 20px;
      color: #fff;
      background-color: #dfe6da;
      border: none;
      border-radius: 5px;
      text-align: center;
      text-decoration: none;
      /* font-size: 1em; */
    }
    .btn-read-more:hover {
      background-color: #dfe6da;
    }

    .references {
      /* position: fixed; */
      bottom: 10px;
      right: 10px;
      background-color: #eff5ec;
      padding: 10px;
      border-radius: 5px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); /* Bayangan */
      font-size: 0.9em;
      color: #333;
      font-family: 'Times New Roman', Times, serif;
    }

    
    .references a {
      color: #0066cc; /* Warna link */
      text-decoration: none;
    }
    
    .references a:hover {
      text-decoration: underline; /* Underline saat hover */
    }
  </style>

<h5>Pentingnya Kesiapan Mental Sebelum Menikah</h5>

  <div class="highlight">
    <p>Kesiapan mental adalah langkah penting untuk membangun hubungan yang sehat dan memperkuat pondasi emosional. Hal ini dicapai melalui pemahaman diri, komitmen, komunikasi yang efektif dan kesiapan menghadapi tantangan. Dengan kesiapan ini, Anda dapat mempersiapkan diri untuk pernikahan yang bahagia dan harmonis. Pernikahan adalah perjalanan bersama, jadi teruslah belajar, tumbuh, dan beradaptasi sepanjang hidup Anda bersama pasangan.</p>
  </div>
  <div style="text-align:center;">
  <img src="{{ asset('/lte/dist/img/KONTEN1.jpeg') }}" alt="Descriptive Image" style="width:430px; height:auto; margin-bottom:10px;">
  <p style="margin-top:1px; font-size:0.6em; color:#555;">Sumber: Album Prewedding yang Syahdu dan Lembut dengan Sentuhan Gaya Modern Heritage - Bridestory Blog</p>
</div>

  <div class="konten">
  <ul>
    <li><strong>Mengenali dan Menerima Diri Sendiri : </strong> Penting untuk memiliki pemahaman yang jelas tentang siapa diri Anda, keinginan, nilai-nilai, dan harapan dalam hidup. Keterampilan ini membantu Anda mengenal diri sendiri dengan baik, dan memungkinkan Anda untuk berkomunikasi secara jujur dengan pasangan tentang kebutuhan dan harapan Anda.</li>
    <li><strong>Memahami Komitmen : </strong> Pernikahan adalah janji untuk saling mendukung dan bertahan dalam suka dan duka. Memahami arti komitmen ini adalah kunci untuk menghadapi tantangan yang mungkin muncul dalam pernikahan. Penting untuk memiliki kesediaan untuk bekerja sama dan berkomitmen untuk membangun hubungan yang sehat dan berkelanjutan.</li>
    <li><strong>Mengelola Konflik dengan Baik : </strong> Konflik adalah bagian alami dari setiap hubungan. Kesiapan mental sebelum menikah mencakup kemampuan untuk mengelola konflik dengan baik dan berkomunikasi secara efektif. Belajar mendengarkan dengan empati, menghormati pandangan pasangan, dan mencari solusi yang saling menguntungkan adalah keterampilan penting yang akan membantu memperkuat ikatan Anda.</li>
    <li><strong>Membangun Kepercayaan : </strong> Kepercayaan adalah pondasi yang kuat dalam setiap hubungan. Sebelum menikah, penting untuk membangun kepercayaan yang saling menguntungkan antara Anda dan pasangan. Ini melibatkan keterbukaan, kejujuran, dan kesetiaan. Menunjukkan komitmen untuk mempertahankan kepercayaan dan menghormati privasi pasangan juga penting dalam menjaga hubungan yang sehat.</li>
    <li><strong>Mempersiapkan Peran Baru : </strong> Pernikahan membawa perubahan peran dan tanggung jawab baru dalam hidup Anda. Mempersiapkan mental untuk peran sebagai pasangan, pendamping, dan mungkin orang tua di masa depan adalah bagian penting dari kesiapan sebelum menikah. Hal ini melibatkan membahas harapan, tujuan, dan tanggung jawab bersama dalam pernikahan.</li>
    <li><strong>Mencari Dukungan : </strong> Membangun hubungan yang sehat membutuhkan dukungan dari lingkungan sosial dan keluarga. Sebelum menikah, penting untuk memiliki jaringan dukungan yang kuat. Bicarakan dengan teman dekat, keluarga, atau konselor yang tepercaya untuk mendapatkan perspektif dan saran yang berharga.</li>
    <li><strong>Menyadari Realitas Perkawinan : </strong> Penting untuk memiliki pemahaman realistis tentang pernikahan. Tidak semua hari akan menjadi indah, dan pernikahan membutuhkan kerja keras dan komitmen yang berkelanjutan. Memahami bahwa ada tantangan, pengorbanan, dan kompromi yang terlibat dalam pernikahan akan membantu Anda lebih siap menghadapinya.</li>
  </ul>
  </div>
  
  <h5>Saran Psikolog untuk Persiapkan Mental Sebelum Menikah</h5>
  <div class="highlight">
    <p>Menurut Ega Alfath, terdapat tiga formula utama yang dapat membantu mempersiapkan mental sebelum menikah dan dalam menjalani kehidupan pernikahan ke depannya. Formula ini sangat berguna untuk menghadapi berbagai dinamika dalam rumah tangga. Berikut adalah penjelasan mengenai ketiga formula tersebut  :</p>
  </div>
  <div style="text-align:center;">
  <img src="{{ asset('/lte/dist/img/KONTEN5.jpeg') }}" alt="Descriptive Image" style="width:430px; height:auto; margin-bottom:10px;">
  <p style="margin-top:1px; font-size:0.6em; color:#555;">Sumber: Dira dan Roy - Jakarta - Tentang Senja</p>
</div>
  <div class="konten">
  <ul>
    <strong>1. Sadar</strong>
    <div class="paragraf">
    <P>Anda harus menyadari bahwa setiap orang mempunyai kekurangan dan kelebihan, termasuk pasangan, keluarga besar, dan teman-teman dari pasangan. Kekurangan tidak hanya ada pada mereka, tetapi juga pada diri Anda. Bila ada konflik yang muncul berulang kali, sebaiknya Anda bersama pasangan sadar diri mencari penyebabnya untuk mengantisipasi atau menyelesaikan masalah itu.</p>
    </div>
    <strong>2. Tahu</strong> 
    <div class="paragraf">
    <P>Tak hanya mengetahui perasaan yang saling mencintai, tapi kita juga perlu mengetahui tentang kebutuhan, harapan, dan visi pernikahan yang Anda dan pasangan miliki untuk menyelaraskan tujuan hidup. Memiliki pengetahuan tentang ilmu pernikahan juga perlu untuk mengedukasi diri.</p>
    </div>
    <strong>3. Mau</strong> 
    <div class="paragraf">
    <P>Anda dan pasangan harus mau berjuang dan komitmen bersama pasangan. Berjuang untuk saling memperbaiki diri, memaafkan kesalahan diri dan pasangan dalam hubungan. Mau terbuka mengenai apa yang membuat kita nyaman dan tidak. Pertikaian hebat dalam kehidupan berpasangan seringkali terjadi bukan karena suatu masalah besar saja, tetapi ketidaknyamanan yang kecil dan menumpuk dalam hubungan.</p>
    </div>
  </ul>
  </div>

  <h5>Faktor-faktor yang Mempengaruhi Kesiapan Menikah</h5>
  <div class="highlight">
    <p>Degenova (2008) menyatakan bahwa ada beberapa faktor yang dapat mempengaruhi kesiapan seseorang untuk menikah. Faktor-faktor tersebut meliputi  :</p>
  </div>
  <div class="konten">
  <ul>
    <li><strong>Usia saat menikah : </strong> Pasangan yang menikah pada usia remaja (belasan) biasanya menikah dikarenakan hamil diluar pernikahan, sehingga biasanya mereka mengabaikan pendidikan dan menerima status sebagai pegawai rendahan.</li>
    <li><strong>Level kedewasaan dari pasangan yang akan menikah : </strong> Remaja biasanya tidak cukup dewasa untuk menghadapi hubungan pernikahan dikarenakan kurangnya kemampuan komunikasi, rasa cemburu atau kurangnya rasa percaya.</li>
    <li><strong>Waktu menikah : </strong> Beberapa pasangan menikah pada waktu yang tidak sesuai dengan waktu yang telah mereka rencanakan, sehingga mereka terkadang merasa kurang bergairah dengan perkawinan itu sendiri.</li>
    <li><strong>Motivasi untuk menikah : </strong> Kebanyakan  individu  menikah  dengan  alasan  pemenuhan  cinta, companionship, dan keamanan namun ada pula yang menikah dengan tujuan  untuk  dapat  terbebas  dari  situasi  hidup  yang  tidak menyenangkan, untuk menyembuhkan ego yang rusak dalam hal rebound.</li>
    <li><strong>Kesiapan untuk eksklusivitas seksual : </strong> Biasanya pasangan memiliki keinginan terhadap eksklusivitas seksual. Jika seseorang tidak memiliki kesiapan terhadap hal ini maka kemungkinan mereka tidak siap untuk menikah.</li>
    <li><strong>Emansipasi emosional dari orangtua : </strong> Individu harus sudah siap untuk memberikan penghasilan dan afeksi kepada pasangannya bukan kepada orangtua.</li>
  </ul>
  </div>
  <div style="text-align:center;">
  <img src="{{ asset('/lte/dist/img/KONTEN6.jpg') }}" alt="Descriptive Image" style="width:430px; height:auto; margin-bottom:10px;">
  <p style="margin-top:0px; font-size:0.6em; color:#555;">Sumber: Best Engagement Photos of 2019</p>
</div>
  <div class="highlight">
    <p>Blood (1978) mengelompokkan aspek-aspek kesiapan untuk menikah menjadi dua kategori, yaitu kesiapan pribadi (personal) dan kesiapan situasional (circumstantial). Berikut adalah penjelasan masing-masing kategori tersebut  :</p>
  </div>
  <div class="konten">
  <ul>
    <strong>1. Kesiapan Pribadi (Personal)</strong>
    <div class="paragraf">
    <strong>Kematangan Emosi : </strong> Kematangan emosi adalah konsep normatif dalam perkembangan psikologis yang berarti bahwa seorang individu telah menjadi seorang yang dewasa. Individu yang telah matang secara emosi maka sudah dapat dikatakan dewasa. Orang dewasa adalah orang yang telah mengembangkan kemampuannya untuk membangun dan memelihara hubungan pribadi. Individu dewasa dapat dikatakan telah memiliki kematangan emosi adalah kemampuan untuk membangun dan mempertahankan hubungan pribadi, mampu mengerti perasaan orang lain (empati), mampu mencintai dan dicintai, mampu untuk memberi dan menerima, serta sanggup membuat komitmen jangka panjang.
    </div>
    <div class="paragraf">
    <strong>Kesiapan Usia : </strong> Pada dasarnya usia dikaitkan dengan kedewasaan atau kematangan, karena proses untuk menjadi individu yang matang atau dewasa membutuhkan waktu sampai individu tersebut menjadi dewasa secara emosi atau pribadi. Individu yang telah dewasa dari segi usia tentunya akan memutuskan untuk menikah. Semakin tua usia seseorang maka semakin dewasa pemikiran seseorang. Sebaliknya, semakin muda usia seseorang maka semakin sulit untuk mengatasi emosi-emosinya.
    </div>
    <div class="paragraf">
    <strong>Kematangan Sosial : </strong> Kematangan sosial dapat dilihat dari dua hal, yaitu pengalaman berkencan (enough dating) dan pengalaman hidup sendiri (enough single life). Pengalaman berkencan yang dilihat dengan adanya keinginan untuk mengabaikan lawan jenis yang tidak di kenal secara dekat, namun membuat komitmen dalam membangun hubungan hanya dengan seseorang yang khusus yang telah dikenal. Selain seseorang telah cukup melakukan kencan, seseorang juga memerlukan waktu untuk hidup mandiri sementara waktu tanpa harus bergantung kepada orang tua. Seorang individu, khususnya wanita merasa perlu untuk membuktikan pada diri mereka sendiri, orang tua, dan pasangan bahwa mereka mampu untuk mengambil keputusan dan mengatur takdirnya sendiri.
    </div>
    <div class="paragraf">
    <strong>Kesehatan Emosional : </strong> Permasalahan emosional yang dimiliki manusia di antaranya adalah kecemasan, merasa tidak nyaman, curiga, dan lain-lain. Jika hal tersebut berada tetap pada diri seseorang maka ia akan sulit menjalin hubungan dengan orang lain. Masalah emosi biasanya menjadi tanda dari ketidak-matangan, yaitu bersikap posesif, ketidakmampuan bertanggung jawab dan tidak dapat diprediksi.
    </div>
    <div class="paragraf">
    <strong>Kesiapan Model Peran : </strong> Banyak orang belajar bagaimana menjadi suami dan istri yang baik dengan melihat figur ayah dan ibu mereka. Kehidupan pernikahan harus dijalani dengan mengetahui apa saja peran individu yang telah menikah sebagai suami istri. Orang tua yang memiliki figur suami dan istri yang baik dapat mempengaruhi kesiapan menikah anak-anak mereka.
    </div>
    <strong>2. Kesiapan Situasi (Circumstantial)</strong>
    <div class="paragraf">
    <strong>Kesiapan Finansial : </strong> Kesiapan finansial tergantung dari nilai-nilai yang dimiliki masing-masing pasangan. Pasangan yang menikah di usia muda yang masih memiliki penghasilan yang rendah, maka sedikit banyak masih memerlukan bantuan materi dari orang tua. Pasangan seperti ini dikatakan belum mampu mandiri sepenuhnya dalam mengurus rumah tangga yang memungkinkan akan menghadapi masalah yang lebih besar nantinya. Semakin tinggi pendapatan seseorang maka semakin besar kemungkinan ia untuk menikah. Pernikahan yang masih mendapat bantuan dari keluarga atau orang tua dapat mempengaruhi hubungan pasangan dalam rumah tangga.
    </div>
    <div class="paragraf">
    <strong>Kesiapan Waktu : </strong> Persiapan sebuah pernikahan akan berlangsung baik jika masing-masing pasangan diberikan waktu untuk mempersiapkan segala hal, meliputi persiapan sebelum maupun setelah pernikahan. Masing-masing pasangan perlu mempersiapkan rencana-rencana untuk pernikahan, bulan madu, dan tahun-tahun pertama pernikahan. Persiapan rencana yang tergesa-tergesa akan mengarah pada persiapan pernikahan yang buruk dan memberi dampak yang buruk pada awal-awal pernikahan.
    </div>
  </ul>
  </div>

  <div class="references">
    <p><strong>Sumber : </strong></p>
    <ul>
      <li><a href="https://herminahospitals.com/id/articles/kesiapan-mental-sebelum-menikah-memperkuat-pondasi-emosional-untuk-hubungan-yang-sehat.html" target="_blank">Kesiapan Mental Sebelum Menikah - Hermina Hospitals</a></li>
      <li><a href="https://kumparan.com/kumparanwoman/saran-psikolog-untuk-persiapkan-mental-sebelum-menikah-1rGajocrUy7/full" target="_blank">Saran Psikolog untuk Persiapkan Mental Sebelum Menikah - Kumparan Woman</a></li>
      <li><a href="https://www.kajianpustaka.com/2022/05/kesiapan-menikah.html" target="_blank">Kesiapan Menikah - Kajian Pustaka</a></li>
    </ul>
  </div>
</div>

@endsection
