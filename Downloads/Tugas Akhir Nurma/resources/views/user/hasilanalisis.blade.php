@extends('user.main')

@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap">

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <!-- <h1 class="m-0">Hasil Analisis</h1> -->
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Hasil Analisis</li> -->
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <style>
    body {
      background-color:#eff5ec;
    }
    .content-wrapper {
        background-color: #eff5ec; 
    }

    .result-container {
      width: 400px;
      margin: 0 auto;
      text-align: center;
      font-family: Arial, sans-serif;
    }

    .result {
      width: 700px;
      margin: 0 auto;
      margin left: 50px;
      margin right: 50px;
      text-align: justify;
      font-family: 'Times New Roman', Times, serif;
      word-wrap: break-word;
      padding-top: 40px; /* Menambahkan 20px pada bagian atas */
    }

    .bar-container {
      width: 100%;
      background-color: #e0e0e0;
      border-radius: 25px;
      overflow: hidden;
      position: relative;
    }

    .bar {
      height: 30px;
      border-radius: 25px 0 0 25px;
      position: relative;
      display: flex;
      align-items: center;
      justify-content: center;
      color: black; /* Mengubah warna teks menjadi hitam */
      font-weight: bold;
    }

    .bar span {
      position: absolute;
      left: 50%;
      transform: translateX(-50%);
    }

    .bar-arrow {
      width: 0;
      height: 0;
      border-left: 10px solid transparent;
      border-right: 10px solid transparent;
      border-bottom: 20px solid black;
      position: absolute;
      top: -20px;
      left: 50%;
      transform: translateX(-50%);
      word-wrap: break-word;
      text-align: justify;
    }

    .level-description th {
      background-color: #3498db;
      color: white;
      padding: 12px;
      font-family: Arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
      word-wrap: break-word;
      text-align: justify;
      margin-bottom: 30px;
      line-height: 1.6;
    }

    .level-description td, .level-description th {
      border: 1px solid #ddd;
      padding: 12px;
      text-align: left;
      word-wrap: break-word;
      text-align: justify;
    }

    .level-description tr:nth-child(even) {
      background-color: #f1f1f1;
      word-wrap: break-word;
      text-align: justify;
    }

    .level-description tr:hover {
      background-color: #ddd;
      word-wrap: break-word;
      text-align: justify;
    }

    .header {
      font-family: 'Arial Black', Arial, sans-serif;
      color: #2c3e50; /* Warna biru tua */
      margin-bottom: 10px; /* Jarak bawah antara judul */
    }

    .level {
      font-family: 'Arial Black', Arial, sans-serif;
      padding: 5px 10px;
      border-radius: 5px;
      box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    }

    .level.sedang {
      color: #ff8f00;/* Warna teks oranye tua */
    }

    .level.tinggi {
      color: #27ae60; /* Warna teks hijau */
    }

    .level.rendah {
      color: #e53935; /* Warna teks merah */
    }

    .print-btn {
    position: absolute;
    top: 10px;
    right: 10px;
    background-color: #4f4f4f; /* Warna abu-abu tua */
    font-family: 'Arial Black', Arial, sans-serif;
    color: white;
    border: none;
    padding: 10px 20px;
    cursor: pointer;
    border-radius: 5px;
}

.print-btn:hover {
    background-color: #3e3e3e; /* Warna sedikit lebih gelap saat hover */
}

.faktor th, .faktor td {
            text-align: center; /* Tulisan di tengah */
            padding: 10px; /* Tambahkan padding agar lebih rapi */
            font-family: 'Times New Roman', Times, serif; 
            justify-content: center;
        }

    /* Additional CSS styles for printing */

    .faktor{
  word-wrap: break-word;
  font-family: 'Times New Roman', Times, serif; 
  border: 1px solid black; 
  padding: 2px;
  font-size: 14px;
}
    
    @media print {
      .body {
        background-color: #ffffff;
        background-image: none;
      }
      .print-btn {
    display: none;
  }
      .printable-template {
        display: block;
        margin-top: 5px
      }

      /* .result-container {
        display: none;
      } */

      .result-container {
      /* width: 00px; */
      margin: 0 auto;
      /* text-align: center; */
      font-family: 'Times New Roman', Times, serif;
      display: none;
      font-size: 20px;
      /* padding-left: 50px; */
    }

    .result {
      margin: 0 auto;
      /* text-align: justify; */
      font-family: 'Times New Roman', Times, serif;
      word-wrap: break-word;
      font-size: 20px;
      /* padding-top: 40px; Menambahkan 20px pada bagian atas */
      /* padding-left: 50px; */
    }

      .btn {
        display: none;
      }

      .logo-img {
        width: 400px; /* Mengatur ulang ukuran logo pada halaman cetak */
        height: auto;
        margin-bottom: 0px; /* Mengurangi margin bawah pada logo */
        margin-top: 100px;
      }

      .title {
        font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
        color: black; /* Warna teks menjadi hitam */
        margin-bottom: 50px; /* Jarak bawah antara judul */
        margin-top: 0px; /* Mengurangi margin atas untuk mendekatkan dengan logo */
        text-align: center;
        font-weight: bold;
        padding-left: 100px;
        padding-right: 80px;
      }

    .title2 {
    font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
    color: black; /* Warna teks menjadi hitam */
    margin-bottom: 0px; /* Jarak bawah antara judul */
    margin-top: 10px;
    text-align: left;
    padding-left: 100px;
    padding-Right: 80px;
    /* line-height: 1.6; */
    display: inline-block;
    font-weight: bold;
    }

    .spaced-text {
    line-height: 1.6; /* Atur jarak antar baris teks */
    font-size: 20px;
    margin-top: 30px;
    margin-bottom: 0px;
    text-align: justify;
}


    .table {
    border-collapse: collapse;
}
.saran{
  margin-top:40px;
}


tbody tr:first-child {
    border-top: none; /* Hapus garis atas dari baris pertama */
}


    }

    @media screen {
      .printable-template {
        display: none;
      }
    }
  </style>

  @php
    $hasil = $hasil; // Skor yang didapat dari analisis
    $nama_lengkap = $nama_lengkap;
    $tanggal_analisis = $tanggal_analisis;

    // Tentukan panjang bar berdasarkan skor
    $bar_length = ($hasil / 100) * 100; // Asumsikan skor maksimal adalah 100

    // Tentukan warna bar dan level berdasarkan skor
    if ($hasil < 50) {
        $bar_color = '#e53935'; // Merah untuk level rendah
        $level = "RENDAH";
        $description = "Berdasarkan hasil analisis, Anda menunjukkan kesiapan psikologis pada tingkat yang 
        rendah yaitu sebesar $hasil% untuk memasuki jenjang pernikahan. Hasil ini 
        mengindikasikan bahwa masih terdapat banyak aspek penting yang perlu 
        Anda perhatikan dan kembangkan lebih lanjut sebelum siap untuk menjalani 
        kehidupan berumah tangga. Beberapa aspek yang perlu mendapat perhatian 
        khusus antara lain kematangan emosi, kesiapan peran, manajemen 
        konflik, dan kesiapan finansial. Selain itu, pemahaman  
        mengenai komitmen, tanggung jawab, serta komunikasi dalam 
        pernikahan juga menjadi hal yang penting untuk Anda kuasai lebih dalam.";
        $saran1 = "Lakukan Evaluasi Diri : Melakukan evaluasi diri secara spesifik untuk mengidentifikasi aspek-aspek yang Anda rasa kurang siap, baik dalam hal emosi maupun kesiapan peran.";  
        $saran2 = "Carilah Bantuan Profesional : Pertimbangkan untuk mendapatkan bantuan dari konselor atau psikolog untuk mengatasi masalah yang mungkin Anda hadapi.";
        $saran3 = "Diskusi dengan Pasangan : Bicarakan hasil ini dengan pasangan Anda. Keterbukaan dan dukungan satu sama lain sangat penting dalam hal ini.
        Ambil waktu yang cukup untuk memperbaiki dan mengembangkan diri Anda, serta fokus pada pembangunan diri dan hubungan Anda sebelum melangkah ke jenjang pernikahan. ";
    } elseif ($hasil < 80) {
        $bar_color = '#ff8f00'; // Oranye untuk level sedang
        $level = "SEDANG";
        $description = "Berdasarkan hasil analisis, Anda menunjukkan kesiapan psikologis pada tingkat yang 
        sedang yaitu sebesar $hasil% untuk memasuki jenjang pernikahan. 
        Ini menunjukkan bahwa Anda sudah memiliki beberapa kualitas yang diperlukan 
        untuk kehidupan pernikahan, namun masih terdapat beberapa aspek yang 
        memerlukan perhatian dan pengembangan lebih lanjut.
        Beberapa aspek yang perlu Anda fokuskan untuk ditingkatkan antara lain 
        manajemen konflik dan pemahaman mengenai 
        peran serta tanggung jawab dalam pernikahan. Berikut adalah hasil analisis Anda berdasarkan berbagai faktor yang mempengaruhi kesiapan psikologis sebelum menikah :";
        $saran1 = "Identifikasi : Sebagai langkah awal, identifikasi secara spesifik pada aspek mana Anda mungkin merasa kurang siap. Perhatikan apakah itu terkait dengan pengungkapan emosi, kesiapan peran, atau kemampuan berkomunikasi.";  
        $saran2 = "Kembangkan Kualitas : Setelah mengidentifikasi aspek yang membutuhkan perbaikan, kembangkan kualitas Anda pada aspek-aspek tersebut. Carilah cara untuk meningkatkan kualitas Anda, seperti membaca buku yang relevan, mengikuti workshop mengenai pernikahan, atau mendapatkan konseling dari seorang profesional.";
        $saran3 = "Diskusi dengan Pasangan : Bicarakan hasil ini dengan pasangan Anda dan buat rencana bersama untuk mengatasi aspek yang perlu diperbaiki. Jangan ragu untuk terus berupaya meningkatkan kesiapan Anda sebelum melangkah ke jenjang pernikahan. Fokus pada pembangunan diri dan pengembangan hubungan yang sehat akan membantu Anda mencapai kesiapan yang optimal.";
    } elseif ($hasil <= 100) {
        $bar_color = '#27ae60'; // Hijau untuk level tinggi
        $level = "TINGGI";
        $description = "Berdasarkan hasil analisis, Anda menunjukkan kesiapan psikologis pada tingkat yang 
        tinggi yaitu sebesar $hasil% untuk memasuki jenjang pernikahan. 
        Anda telah memiliki banyak kualitas yang diperlukan untuk membangun 
        hubungan yang sehat dan bahagia dengan pasangan Anda. Ini termasuk 
        kematangan emosi, kesiapan peran, dan kemampuan manajemen 
        konflik yang baik. Kesiapan Anda dalam hal finansial dan pemahaman 
        mengenai komitmen serta tanggung jawab dalam pernikahan juga sudah 
        berada pada tingkat yang baik.";
        $saran1 = "Pertahankan Kualitas : Terus kembangkan dan pertahankan kualitas yang telah Anda miliki. Komunikasi yang terbuka dan jujur, serta kemampuan beradaptasi, akan terus menjadi pilar penting dalam pernikahan Anda.";  
        $saran2 = "Pelajari Lebih Lanjut : Meskipun Anda berada dalam posisi yang sangat baik, selalu luangkan waktu untuk terus melakukan pengembangan diri. Teruslah belajar, mencari tahu, dan mencari cara untuk memperdalam hubungan Anda dengan pasangan.";
        $saran3 = "Terbuka untuk Konseling Pranikah : Meskipun Anda telah menunjukkan kesiapan yang tinggi, mengikuti konseling pranikah dapat memberikan wawasan tambahan dan memperkuat ikatan Anda dengan pasangan sebelum memasuki pernikahan. Proses ini dapat membantu mengidentifikasi aspek yang perlu ditingkatkan dan memfasilitasi komunikasi yang lebih baik.";
    } 
  @endphp

  <div class="result-container">
    <h3 class="header">Tingkat Kesiapan Psikologis</h3>
    <h4 class="level {{ strtolower($level) }}">{{ $level }}</h4>

    <img src="{{ asset('/lte/dist/img/HASIL5.png') }}" alt="Descriptive Image" style="width:200px; height:auto; margin-bottom:10px;">
    <div class="bar-container">
        <div class="bar" style="width: {{ $bar_length }}%; background-color: {{ $bar_color }};">
            <span>{{ $hasil }}%</span>
        </div>
    </div>
    <table style="margin-top: 10px;" border="1" class="faktor">
        <thead>
            <tr>
                <th>Faktor</th>
                <th>Hasil Analisis</th>
            </tr>
        </thead>
        <tbody>
            @foreach($results as $result)
                <tr>
                    <td>{{ $result['faktor'] }}</td>
                    <td>Anda mendapatkan skor <strong>{{ $result['total_cf'] }}</strong> dari total maksimal skor <strong>{{ $result['cfmaksimal'] }}</strong></td>
                </tr>
              
            @endforeach
        </tbody>
    </table>
</div>

<!-- Printable template -->
<div class="printable-template">
    <div class="logo">
        <img src="{{ asset('/lte/dist/img/logo2.png') }}" alt="Logo" class="logo-img">
        <!-- <div class="title2">
          <h5>Nama Lengkap    : {{ $nama_lengkap }}</h5>
          <h5>Jenis Kelamin    : {{ $jenis_kelamin }}</h5>
          <h5>Tanggal Analisis : {{ $tanggal_analisis }}</h5>
        </div> -->
    </div>
    <h2 class="title">HASIL ANALISIS KESIAPAN PSIKOLOGIS</h2>
    <div class="title2">
        <table>
            <tbody>
                <tr>
                    <td><h5><strong>Nama Lengkap </strong></h5></td>
                    <td><h5><strong>:</strong></h5></td>
                    <td><h5><strong>{{ $nama_lengkap }}</strong></h5></td>
                </tr>
                <tr>
                    <td><h5><strong>Jenis Kelamin</strong></h5></td>
                    <td><h5><strong>:</strong></h5></td>
                    <td><h5><strong>{{ $jenis_kelamin }}</strong></h5></td>
                </tr>
                <tr>
                    <td><h5><strong>Tanggal Analisis</strong></h5></td>
                    <td><h5><strong>:</strong></h5></td>
                    <td><h5><strong>{{ $tanggal_analisis }}</strong></h5></td>
                </tr>
            </tbody>
        </table>
        <h5 class="spaced-text">
            Pernikahan merupakan komitmen hidup yang penting. Oleh karena itu, sangat penting untuk memastikan kesiapan
            psikologis sebelum memasuki jenjang tersebut. Berikut adalah hasil analisis dan saran untuk mempersiapkan diri
            secara optimal. Dengan komitmen, upaya yang terus-menerus, dan dukungan yang tepat, kami yakin Anda akan
            mampu membangun rumah tangga yang harmonis dan bahagia bersama pasangan Anda di masa depan.
        </h5>
    </div>

    <!-- Add more content as needed -->

</div>

<div class="result">
    <h5><strong>KESIMPULAN</strong></h5>
    <p class="level-description">{{ $description }}</p>
    <!-- <div style="display: flex; justify-content: center;"> -->
    <!-- <table style="margin-top: 10px;" border="1" class="faktor">
        <thead>
            <tr>
                <th>Faktor</th>
                <th>Hasil Analisis</th>
            </tr>
        </thead>
        <tbody>
            @foreach($results as $result)
                <tr>
                    <td>{{ $result['faktor'] }}</td>
                    <td>{{ $result['total_cf'] }}</td>
                </tr>
              
            @endforeach
        </tbody>
    </table>
   -->
    <div class="saran">
    <h5 style="margin-top: 30px;"><strong>SARAN</strong></h5>
    <table class="level-description">
        <tr>
            <td>1.</td>
            <td>{{ $saran1 }}</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>{{ $saran2 }}</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>{{ $saran3 }}</td>
        </tr>
    </table>
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Print button -->
                <button class="print-btn" onclick="printTemplate()">CETAK</button>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

  <!-- /.content -->
</div>

<script>
 function printTemplate() {
  var printableTemplate = document.querySelector('.printable-template');
  var resultContainer = document.querySelector('.result-container');
  var printBtn = document.querySelector('.print-btn');
  var originalDisplay = resultContainer.style.display;

  // Hide the result-container before printing
  resultContainer.style.display = 'none';

  // Hide the print button
  printBtn.style.display = 'none';

  // Print only the printable-template
  window.print();

  // Restore the original display property of result-container
  resultContainer.style.display = originalDisplay;

  // Restore the original display property of print button
  printBtn.style.display = 'block';
}
</script>
@endsection
