@extends('layout.main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <style>
      .content-wrapper {
        background-color: #dfe6da; /* Warna pink soft */ /* Ubah kode warna sesuai preferensi Anda */
    color: white; /* Warna teks yang kontras dengan latar belakang */
}
</style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small box -->
          <div class="small-box bg-secondary">
              <div class="inner">
              <h3>
              {{ $pakar = App\Models\pakar::get()->count() }}
</h3>
                <p>Total Pakar</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{ route('admin.indexpakar') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <h3>
    {{ $data = App\Models\User::get()->count() }}
</h3>
                <p>Total User</p>
              </div>
              <div class="icon">
              <i class="ion ion-person"></i>
              </div>
              <a href="{{ route('admin.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $pertanyaan = App\Models\kesiapanpsikologis::get()->count() }}<sup style="font-size: 20px"></sup></h3>
                <p>Total Pertanyaan</p>
              </div>
              <div class="icon">
              <i class="fas fa-list"></i>
              </div>
              <a href="{{ route('admin.kesiapanpsikologis') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
              <h3>
    {{ $analisis = App\Models\jawaban::get()->count() }}
</h3>

                <p>Total Konsultasi</p>
              </div>
              <div class="icon">
              <i class="fas fa-history"></i> <!-- Ini adalah contoh ikon riwayat -->
              </div>
              <a href="{{ route('admin.jawabanuser') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection