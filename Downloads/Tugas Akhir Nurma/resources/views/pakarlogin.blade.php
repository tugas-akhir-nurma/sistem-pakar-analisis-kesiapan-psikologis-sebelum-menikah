<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login Admin</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Great+Vibes&display=swap">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="logo">
            <img src="{{ asset('/lte/dist/img/logo2.png') }}" alt="Logo" class="logo-img">
        </div>
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="{{ route('pakarlogin') }}" class="h2"><b>WELCOME</b>admin!</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Halo, masuk untuk menjadi admin</p>

      <form action="{{ route('pakarlogin-proses') }}" method="post">
        @csrf
        @error('email')
            <small class="form-text text-danger">{{ $errors->first('email') }}</small>
          @enderror
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Masukkan Email" value="{{ old('email') }}">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        
        @error('password')
            <small class="form-text text-danger">{{ $errors->first('password') }}</small>
          @enderror
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Masukkan Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<style>
.card-outline.card-primary {
  padding: 6px 10px;
    margin-bottom: 0;
    border: none;
    background-color: rgba(0,0,0,.4);
    box-shadow: 0 0 30px rgba(255,255,255,.3);
    border-bottom: 2px solid #2979ff;
    color: #fff;
    margin-top: 50px;
}
.text-center {
  color: white;
}

.form-control{
  background-color: transparent;
}
*{
    margin-bottom: 0;
    padding: 0;
    outline: 0;
    font-family: 'Open Sans', sans-serif;
}
body{
    height: 100vh;
    background-image: url("{{ asset('/lte/dist/img/LOGINBARU2.jpeg') }}" );
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
}
.logo {
            position: absolute;
            top: 0;
            left: 0;
            padding: 20px;
        }
        
        .logo-img {
            max-width: 350px; /* Sesuaikan lebar logo */
            height: auto;
        }
</style>

@if($message = Session::get('failed'))
<script>
Swal.fire({
  icon: "error",
  title: "Oops...",
  text: "{{ $message }}",
});         
</script>                                  
@endif

@if($message = Session::get('success'))
<script>
Swal.fire({
  position: "center-center",
  icon: "success",
  title: "{{ $message }}",
  showConfirmButton: false,
  timer: 1500
});
</script>                                  
@endif

</body>
</html>
