@extends('layout.main')

@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <div class="row mb-4">
          <div class="col-sm-12 text-center">
            <h3 class="title">FORM EDIT PAKAR</h3>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit User</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <style>
    .title {
            font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
            color: black; /* Warna teks menjadi hitam */
            margin-bottom: 0px; /* Jarak bawah antara judul */
            margin-top: 0px; /* Mengurangi margin atas untuk mendekatkan dengan logo */
            text-align: center; /* Menempatkan teks di tengah */
            font-weight: bold; /* Membuat teks tebal */
        }
        .content-wrapper {
        background-color: #dfe6da; /* Warna pink soft */ /* Ubah kode warna sesuai preferensi Anda */  
      }
      .container-fluid {
        max-width: 800px; /* Set the maximum width of the container */
        margin: 0 auto; /* Center the container */
        padding: 20px; /* Add some padding */
      }
      .card{
      background-color: #eff5ec;
      font-family: 'Times New Roman', Times, serif; 
    }
    </STYLE>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
            <form action="{{ route('admin.updatepakar',['id' => $pakar->id]) }}" method="POST">
                @csrf
                @method('PUT')
        <div class="row">
          <!-- left column -->
          <div class="col-md-6 offset-md-3"> <!-- Add offset-md-3 to center the column -->
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Form Edit Data Pakar</h3> -->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                <div class="form-group">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $pakar->email }}" placeholder="Enter email"> 
                    @if ($errors->has('email'))
                        <small class="form-text text-danger">{{ $errors->first('email') }}</small>
                    @endif
                </div>
                  <div class="form-group">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    @if ($errors->has('password'))
                        <small class="form-text text-danger">{{ $errors->first('password') }}</small>
                    @endif
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
  <button type="submit" class="btn btn-primary float-right">SUBMIT</button>
</div>
                </div>
                </div>
                </div>
              </form>
        </form>
      </div><!-- /.container-fluid -->
    </section>
  </div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if($message = Session::get('success'))
<script> 
Swal.fire({
  position: "center-center",
  icon: "success",
  title: "{{ $message }}",
  showConfirmButton: false,
  timer: 2500
});
</script>                                  
@endif
@endsection