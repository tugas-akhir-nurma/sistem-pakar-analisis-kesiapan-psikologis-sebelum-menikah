@extends('layout.main')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.css" />
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha384-uxLZ81p1VZ8NvdLyHwr7BtAhpq/oNfgSwWg9x3SgqI3g0n2kNq/GjvWSwRnVN+Zr" crossorigin="anonymous">
  <!-- Jquery -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js"></script>

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12">
          <h3 class="title">RIWAYAT KONSULTASI</h3>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Data User</li> -->
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div>
  </div>
  <!-- /.content-header -->
  <style>
    .table {
      width: 100%;
      /* Sesuaikan lebar tabel sesuai kebutuhan */
      text-align: left;
    }

    .faktor-column {
      white-space: pre-line;
      word-wrap: break-word;
      max-width: 140px;
    }
    .card{
      background-color: #eff5ec;
      font-family: 'Times New Roman', Times, serif; 
    margin-right: 30px; /* Adds left and right margins */
    margin-left:30px; 
    text-align: left;
    }
    .content-wrapper {
      background-color: #dfe6da;
      /* Warna pink soft */
      /* Ubah kode warna sesuai preferensi Anda */
    }

    .top-right-button {
      background-color: #343a40;
      /* Abu-abu tua */
      border-color: #343a40;
      /* Abu-abu tua */
      color: white;
      margin-top: 5px;
      /* Adjust this value as needed */
    }

    .btn-dark-gray {
      background-color: #343a40;
      /* Abu-abu tua */
      border-color: #343a40;
      /* Abu-abu tua */
      color: white;
      font-family: 'Times New Roman', Times, serif;
    }

    .title {
      font-family: 'Times New Roman', Times, serif;
      /* Mengubah font agar lebih formal untuk judul cetak */
      color: black;
      /* Warna teks menjadi hitam */
      margin-bottom: 0px;
      /* Jarak bawah antara judul */
      margin-top: 0px;
      /* Mengurangi margin atas untuk mendekatkan dengan logo */
      text-align: center;
      /* Menempatkan teks di tengah */
      font-weight: bold;
      /* Membuat teks tebal */
    }

    /* Optional: Styling the button */
    .print-button {
      background-color: transparent;
      border: none;
      cursor: pointer;
    }

    /* Optional: Styling the icon */
    .print-icon {
      color: black;
      /* Change color as needed */
    }
    #serverside th,
#serverside td {
    text-align: center;
    vertical-align: middle; /* Ensures text is vertically centered as well */
    background-color: #eff5ec;
}

/* Optional: Add some styling to the table */
#serverside {
    width: 100%; /* Adjust this value to set the table width as needed */
    border-collapse: collapse; /* Ensures there is no space between table cells */
    text-align: center;
}

#serverside th,
#serverside td {
    padding: 10px; /* Adds some padding for better readability */
    border: 1px solid #ddd; /* Adds a border to the cells */
    text-align: center;
}

#serverside th {
    background-color: #eff5ec; /* Adds a background color to the header */
}


  </style>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search"> -->
                  <div class="input-group-append">
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap" id="riwayat">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tanggal Analisis</th>
                    <!-- <th>Email</th> -->
                    <th>Nama Lengkap</th>
                    <th>Jenis Kelamin</th>
                    <th>Nilai Akhir</th>
                    <th>Detail</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($analisis as $a)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ \Carbon\Carbon::parse($a->tanggal_analisis)->format('d/m/Y') }}</td>
                    <!-- <td>{{ $a->email }}</td> -->
                    <td>{{ $a->nama_lengkap }}</td>
                    <td>{{ $a->jenis_kelamin }}</td>
                    <td>{{ $a->nilai_akhir }}%</td>
                    <td>
                      <button class="print-button" id="btn_print" value="{{$a->id}}">
                        <i class="fas fa-print fa-lg print-icon"></i>
                      </button>
                    </td>
                    <td>
                      <a data-toggle="modal" data-target="#modal-hapus{{ $a->id }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Hapus</a>
                    </td>
                  </tr>
                  <div class="modal fade" id="modal-hapus{{ $a->id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-warning">
                          <h4 class="title">Konfirmasi Hapus Data</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p>Apakah kamu yakin ingin menghapus riwayat konsultasi <b>{{ $a->nama_lengkap }}</b></p>
                        </div>
                        <form action="{{ route('user.deleteanalisis',['id' => $a->id]) }}" method="POST">
                          @csrf
                          @method('DELETE')
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                            <button type="submit" class="btn btn-danger">Ya, Hapus</button>
                          </div>
                        </form>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if($message = Session::get('success'))
<script>
  Swal.fire({
    position: "center-center",
    icon: "success",
    title: "{{ $message }}",
    showConfirmButton: false,
    timer: 2500
  });
</script>
@endif

<script>
  function printPage() {
    var resultContainer = document.querySelector('.result-container');
    var originalDisplay = resultContainer.style.display;

    // Hide the result-container before printing
    resultContainer.style.display = 'none';

    // Use a timeout to ensure the print dialog opens after hiding the element
    setTimeout(function() {
      window.print();
      // Restore the original display property of result-container
      resultContainer.style.display = originalDisplay;
    }, 100);
  }

  $(document).ready(function() {
    $(document).on('click', '#btn_print', function(e) {
      e.preventDefault();
      var id = $(this).val();
      // console.log(id);
      $.ajax({
        method: "GET",
        url: `{{ url('admin/print-data/${id}') }}`,
        success: function(data) {
          var hasil = data.nilai_akhir;
          var nama = data.nama_lengkap;
          var tanggal_analisis = data.tanggal_analisis;

          var bar = (hasil / 100) * 100;

          var level = '';
          var desk = '';
          var saran1 = '';
          var saran2 = '';
          var saran3 = '';

          if (hasil < 50) {
            level = "RENDAH";
            desk = `Berdasarkan hasil analisis, Anda menunjukkan kesiapan psikologis pada    tingkat yang rendah yaitu sebesar ${hasil}% untuk memasuki jenjang pernikahan. Hasil ini mengindikasikan bahwa masih terdapat banyak aspek penting yang perlu Anda perhatikan dan kembangkan lebih lanjut sebelum siap untuk menjalani kehidupan berumah tangga. Beberapa aspek yang perlu mendapat perhatian khusus antara lain kematangan emosi, kesiapan peran, manajemen konflik, dan kesiapan finansial. Selain itu, pemahaman mengenai komitmen, tanggung jawab, serta komunikasi dalam pernikahan juga menjadi hal yang penting untuk Anda kuasai lebih dalam.`;

            saran1 = `Lakukan Evaluasi Diri : Melakukan evaluasi diri secara spesifik untuk    mengidentifikasi aspek-aspek yang Anda rasa kurang siap, baik dalam hal emosi maupun    kesiapan peran.`;

            saran2 = `Carilah Bantuan Profesional : Pertimbangkan untuk mendapatkan bantuan dari     konselor atau psikolog untuk mengatasi masalah yang mungkin Anda hadapi.`;

            saran3 = `Diskusi dengan Pasangan : Bicarakan hasil ini dengan pasangan Anda. Keterbukaan    dan dukungan satu sama lain sangat penting dalam hal ini. Ambil waktu yang cukup untuk memperbaiki dan mengembangkan diri Anda, serta fokus pada    pembangunan diri dan hubungan Anda sebelum melangkah ke jenjang pernikahan.`;
          } else if (hasil < 80) {
            level = "SEDANG";
            desk = `Berdasarkan hasil analisis, Anda menunjukkan kesiapan psikologis pada tingkat yang sedang yaitu sebesar ${hasil}% untuk memasuki jenjang pernikahan. Ini menunjukkan bahwa Anda sudah memiliki beberapa kualitas yang diperlukan untuk kehidupan pernikahan, namun masih terdapat beberapa aspek yang memerlukan perhatian dan pengembangan lebih lanjut.Beberapa aspek yang perlu Anda fokuskan untuk ditingkatkan antara lain manajemen konflik dan pemahaman mengenai peran serta tanggung jawab dalam pernikahan.`;
            saran1 = "Identifikasi : Sebagai langkah awal, identifikasi secara spesifik pada aspek mana Anda mungkin merasa kurang siap. Perhatikan apakah itu terkait dengan pengungkapan emosi, kesiapan peran, atau kemampuan berkomunikasi.";
            saran2 = "Kembangkan Kualitas : Setelah mengidentifikasi aspek yang membutuhkan perbaikan, kembangkan kualitas Anda pada aspek-aspek tersebut. Carilah cara untuk meningkatkan kualitas Anda, seperti membaca buku yang relevan, mengikuti workshop mengenai pernikahan, atau mendapatkan konseling dari seorang profesional.";
            saran3 = "Diskusi dengan Pasangan : Bicarakan hasil ini dengan pasangan Anda dan buat rencana bersama untuk mengatasi aspek yang perlu diperbaiki. Jangan ragu untuk terus berupaya meningkatkan kesiapan Anda sebelum melangkah ke jenjang pernikahan. Fokus pada pembangunan diri dan pengembangan hubungan yang sehat akan membantu Anda mencapai kesiapan yang optimal.";
          } else if (hasil <= 100) {
            level = "TINGGI";
            desk =  `Berdasarkan hasil analisis, Anda menunjukkan kesiapan psikologis pada tingkat yang tinggi yaitu sebesar ${hasil}%  untuk memasuki jenjang pernikahan. Anda telah memiliki banyak kualitas yang diperlukan untuk membangun hubungan yang sehat dan bahagia dengan pasangan Anda. Ini termasuk kematangan emosi, kesiapan peran, dan kemampuan manajemen konflik yang baik. Kesiapan Anda dalam hal finansial dan pemahaman mengenai komitmen serta tanggung jawab dalam pernikahan juga sudah berada pada tingkat yang baik.`; 
            saran1 = "Pertahankan Kualitas : Terus kembangkan dan pertahankan kualitas yang telah Anda miliki. Komunikasi yang terbuka dan jujur, serta kemampuan beradaptasi, akan terus menjadi pilar penting dalam pernikahan Anda.";  
            saran2 = "Pelajari Lebih Lanjut : Meskipun Anda berada dalam posisi yang sangat baik, selalu luangkan waktu untuk terus melakukan pengembangan diri. Teruslah belajar, mencari tahu, dan mencari cara untuk memperdalam hubungan Anda dengan pasangan.";
            saran3 = "Terbuka untuk Konseling Pranikah : Meskipun Anda telah menunjukkan kesiapan yang tinggi, mengikuti konseling pranikah dapat memberikan wawasan tambahan dan memperkuat ikatan Anda dengan pasangan sebelum memasuki pernikahan. Proses ini dapat membantu mengidentifikasi aspek yang perlu ditingkatkan dan memfasilitasi komunikasi yang lebih baik.";
          }

          // console.log(data);
var print = window.open();
print.document.write(`<html>
  <head>
    <title>Print Data "${data.nama_lengkap}"</title>
  </head>
 
  <body>
      

      <h2 class="title">HASIL ANALISIS KESIAPAN PSIKOLOGIS</h2>
      <div class="title2">
        <table>
          <tbody>
            <tr>
              <td><h5><strong>Nama Lengkap </strong></h5></td>
              <td><h5><strong>:</strong></h5></td>
              <td><h5><strong>${nama}</strong></h5></td>
            </tr>
            <tr>
              <td><h5><strong>Jenis Kelamin</strong></h5></td>
              <td><h5><strong>:</strong></h5></td>
              <td><h5><strong>${data.jenis_kelamin}</strong></h5></td>
            </tr>
            <tr>
              <td><h5><strong>Tanggal Analisis</strong></h5></td>
              <td><h5><strong>:</strong></h5></td>
              <td><h5><strong>${tanggal_analisis}</strong></h5></td>
            </tr>
          </tbody>
        </table>

        <div class="result">
          Pernikahan merupakan komitmen hidup yang penting. Oleh karena itu, sangat penting untuk memastikan kesiapan psikologis sebelum memasuki jenjang tersebut. Berikut adalah hasil analisis dan saran untuk mempersiapkan diri secara optimal. Dengan komitmen, upaya yang terus-menerus, dan dukungan yang tepat, kami yakin Anda akan mampu membangun rumah tangga yang harmonis dan bahagia bersama pasangan Anda di masa depan.
        </div>
      </div>

      <div class="result">
        <h5><strong>KESIMPULAN</strong></h5>
        <p class="level-description">${desk}</p>
        <h5><strong>SARAN</strong></h5>
        <table class="level-description">
          <tr>
            <td>1.</td>
            <td>${saran1}</td>
          </tr>
          <tr>
            <td>2.</td>
            <td>${saran2}</td>
          </tr>
          <tr>
            <td>3.</td>
            <td>${saran3}</td>
          </tr>
        </table>
      </div>
    </div>
  </body>

  <style>
   

      .logo-img {
        width: 450px; /* Mengatur ulang ukuran logo pada halaman cetak */
        height: auto;
        margin-bottom: 0px; /* Mengurangi margin bawah pada logo */
      }
      
      .title {
      text-align: center;
      margin-bottom: 50px;
      margin-top: 50px;
      }

      .result {
      text-align: justify;
        }
          
        .level-description {
            text-align: justify;
        }
        table.level-description {
            width: 100%;
            border-collapse: collapse;
        }
        table.level-description, 
        table.level-description th, 
        table.level-description td {
            border: 1px solid black;
        }
        table.level-description th, 
        table.level-description td {
            padding: 8px;
            text-align: justify;
        }

      
    </style>
</html>`);
print.document.close();
print.print();
        }
      });
    });
  });
</script>

@endsection

@section('scripts')
<script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
<script>
$(document).ready( function () {
    $('#riwayat').DataTable();
} );
</script>
@endsection