@extends('layout.main')

@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12 text-center">
            <h3 class="title">FORM TAMBAH PERTANYAAN</h3>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah User</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
            <form action="{{ route('admin.storekesiapanpsikologis') }}" method="POST">
                @csrf
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Form Tambah Pertanyaan</h3> -->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <style>
    .title {
            font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
            color: black; /* Warna teks menjadi hitam */
            margin-bottom: 0px; /* Jarak bawah antara judul */
            margin-top: 0px; /* Mengurangi margin atas untuk mendekatkan dengan logo */
            text-align: center; /* Menempatkan teks di tengah */
            font-weight: bold; /* Membuat teks tebal */
        }
        .content-wrapper {
        background-color: #dfe6da; /* Warna pink soft */ /* Ubah kode warna sesuai preferensi Anda */  
      }
      .card{
      background-color: #eff5ec;
      font-family: 'Times New Roman', Times, serif; 
    }
    
        </STYLE>
              <form>
                <div class="card-body">
                <div class="form-group">
                <!-- <div class="col-4"> -->
                    <label for="faktor" class="form-label">Faktor</label>
                    <select class="form-control" id="faktor" name="faktor">
                    <option>Kesiapan Emosi</option>
                    <option>Kesiapan Sosial</option>
                    <option>Kesiapan Peran</option>
                    <option>Kesiapan Finansial</option>
                    <option>Kesiapan Spiritual</option>
                    <option>Kesiapan Reproduksi/Seksual</option>
                    <option>Kematangan Usia</option>
                    <option>Kemampuan Berkomunikasi</option>
                    </select>
                    @if ($errors->has('faktor'))
                        <small class="form-text text-danger">{{ $errors->first('faktor') }}</small>
                    @endif
                </div>
                <!-- </div> -->
                <div class="form-group">
                <!-- <div class="col-4"> -->
                <label for="gender" class="form-label">Gender</label>
                <select class="form-control" id="gender" name="gender">
                    <option>Laki-Laki</option>
                    <option>Perempuan</option>
                    <option>Umum</option>
                    </select>
                    @if ($errors->has('gender'))
               <small class="form-text text-danger">{{ $errors->first('gender') }}</small>
               @endif
                </div>
                <div class="form-group">
                    <label for="kesiapan" class="form-label">Pertanyaan Psikologis</label>
                    @if ($errors->has('kesiapan'))
                        <small class="form-text text-danger">{{ $errors->first('kesiapan') }}</small>
                    @endif
                    <input type="text" class="form-control" id="kesiapan" name="kesiapan" placeholder="Masukkan pertanyaan psikologis"> 
                </div>
                <div class="form-group">
                <label for="nilai_bobot" class="form-label">
    Bobot Jawaban
    <span class="text-muted">(Gunakan titik untuk angka desimal, contoh : 0.90)</span>
</label>
   
                <div class="row">
                  <div class="col-2">
                  @if ($errors->has('tidak'))
                        <small class="form-text text-danger">{{ $errors->first('tidak') }}</small>
                    @endif
                    Tidak
                    <input type="text" class="form-control" id="tidak" name="tidak"  placeholder="Enter nilai" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('tidak_tahu'))
                        <small class="form-text text-danger">{{ $errors->first('tidak_tahu') }}</small>
                    @endif
                    Tidak Tahu
                    <input type="text" class="form-control" id="tidak_tahu" name="tidak_tahu" placeholder="Enter nilai" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('sedikit_setuju'))
                        <small class="form-text text-danger">{{ $errors->first('sedikit_setuju') }}</small>
                    @endif
                    Sedikit Setuju
                    <input type="text" class="form-control" id="sedikit_setuju" name="sedikit_setuju" placeholder="Enter nilai" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('cukup_setuju'))
                        <small class="form-text text-danger">{{ $errors->first('cukup_setuju') }}</small>
                    @endif
                    Cukup Setuju
                    <input type="text" class="form-control" id="cukup_setuju" name="cukup_setuju" placeholder="Enter nilai" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('setuju'))
                        <small class="form-text text-danger">{{ $errors->first('setuju') }}</small>
                    @endif
                    Setuju
                    <input type="text" class="form-control" id="setuju" name="setuju" placeholder="Enter nilai" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('sangat_setuju'))
                        <small class="form-text text-danger">{{ $errors->first('sangat_setuju') }}</small>
                    @endif
                    Sangat Setuju
                    <input type="text" class="form-control" id="sangat_setuju" name="sangat_setuju" placeholder="Enter nilai" step="any">
                  </div>  
                </div>
                </div>
                <div class="form-group">
                <label for="nilai_bobot" class="form-label">
    Nilai CFH
    <span class="text-muted">(Gunakan titik untuk angka desimal, contoh : 0.60)</span>
</label>
                    @if ($errors->has('CFH'))
                        <small class="form-text text-danger">{{ $errors->first('CFH') }}</small>
                    @endif
                    <div class="row">
                    <div class="col-2">
                    <input type="text" class="form-control" id="CFH" name="CFH" placeholder="Enter nilai" step="any"> 
                </div>
                </div>
                </div>
                
              <!-- /.card-body -->
                </div>
                

                </div>
                <!-- /.card-body -->

               
                <div class="card-footer">
  <button type="submit" class="btn btn-primary float-right">SUBMIT</button>
</div>
                </div>
                </div>
                </div>
              </form>
        </form>
      </div><!-- /.container-fluid -->
    </section>
  </div>

<!-- jQuery -->
<script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if($message = Session::get('success'))
<script> 
Swal.fire({
  position: "center-center",
  icon: "success",
  title: "{{ $message }}",
  showConfirmButton: false,
  timer: 2500
});
</script>                                  
@endif
@endsection