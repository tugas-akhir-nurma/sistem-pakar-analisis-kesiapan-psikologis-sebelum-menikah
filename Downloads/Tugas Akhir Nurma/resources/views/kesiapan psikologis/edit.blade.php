@extends('layout.main')

@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-12 text-center">
            <h3 class="title">FORM EDIT PERTANYAAN</h3>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit User</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
            <form action="{{ route('admin.updatekesiapanpsikologis',['id' => $pertanyaan->id]) }}" method="POST">
                @csrf
                @method('PUT')
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <!-- <h3 class="card-title">Form Edit Pertanyaan</h3> -->
              </div>
              <!-- /.card-header -->
              <style>
    .title {
            font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
            color: black; /* Warna teks menjadi hitam */
            margin-bottom: 0px; /* Jarak bawah antara judul */
            margin-top: 0px; /* Mengurangi margin atas untuk mendekatkan dengan logo */
            text-align: center; /* Menempatkan teks di tengah */
            font-weight: bold; /* Membuat teks tebal */
        }
        .content-wrapper {
        background-color: #dfe6da; /* Warna pink soft */ /* Ubah kode warna sesuai preferensi Anda */  
      }
      .card{
      background-color: #eff5ec;
      font-family: 'Times New Roman', Times, serif; 
    }
      </style>
              <!-- form start -->
              <form>
                <div class="card-body">
                <div class="form-group">
                <label for="faktor" class="form-label">Faktor</label>
        <select class="form-control" id="faktor" name="faktor">
        <option {{ $pertanyaan->faktor == 'Kesiapan Emosi' ? 'selected' : '' }}>Kesiapan Emosi</option>
                <option {{ $pertanyaan->faktor == 'Kesiapan Sosial' ? 'selected' : '' }}>Kesiapan Sosial</option>
                <option {{ $pertanyaan->faktor == 'Kesiapan Peran' ? 'selected' : '' }}>Kesiapan Peran</option>
                <option {{ $pertanyaan->faktor == 'Kesiapan Finansial' ? 'selected' : '' }}>Kesiapan Finansial</option>
                <option {{ $pertanyaan->faktor == 'Kesiapan Spiritual' ? 'selected' : '' }}>Kesiapan Spiritual</option>
                <option {{ $pertanyaan->faktor == 'Kesiapan Reproduksi/Seksual' ? 'selected' : '' }}>Kesiapan Reproduksi/Seksual</option>
                <option {{ $pertanyaan->faktor == 'Kematangan Usia' ? 'selected' : '' }}>Kematangan Usia</option>
                <option {{ $pertanyaan->faktor == 'Kemampuan Berkomunikasi' ? 'selected' : '' }}>Kemampuan Berkomunikasi</option>
        </select>
        @if ($errors->has('faktor'))
            <small class="form-text text-danger">{{ $errors->first('faktor') }}</small>
        @endif
                </div>
                <div class="form-group">
                <!-- <div class="col-4"> -->
                <label for="gender" class="form-label">Gender</label>
        <select class="form-control" id="gender" name="gender">
        <option {{ $pertanyaan->gender == 'Laki-Laki' ? 'selected' : '' }}>Laki-Laki</option>
                <option {{ $pertanyaan->gender == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
                <option {{ $pertanyaan->gender == 'Umum' ? 'selected' : '' }}>Umum</option>
        </select>
        @if ($errors->has('gender'))
            <small class="form-text text-danger">{{ $errors->first('gender') }}</small>
        @endif
                </div>
                <div class="form-group">
                    <label for="kesiapan" class="form-label">Kesiapan Psikologis</label>
                    <input type="kesiapan" class="form-control" id="kesiapan" name="kesiapan" placeholder="Enter email" value="{{ $pertanyaan->kesiapan }}"> 
                    @if ($errors->has('kesiapan'))
                        <small class="form-text text-danger">{{ $errors->first('kesiapan') }}</small>
                    @endif
                </div>
                <div class="form-group">
                <label for="nilai_bobot" class="form-label">
    Bobot Jawaban
    <span class="text-muted">(Gunakan titik untuk angka desimal, contoh : 0.90)</span>
</label>
                <div class="row">
                  <div class="col-2">
                  @if ($errors->has('tidak'))
                        <small class="form-text text-danger">{{ $errors->first('tidak') }}</small>
                    @endif
                    Tidak
                    <input type="TEXT" class="form-control" id="tidak" name="tidak"  placeholder="Enter nilai" value="{{ $pertanyaan->tidak }}" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('tidak_tahu'))
                        <small class="form-text text-danger">{{ $errors->first('tidak_tahu') }}</small>
                    @endif
                    Tidak Tahu
                    <input type="TEXT" class="form-control" id="tidak_tahu" name="tidak_tahu" placeholder="Enter nilai" value="{{ $pertanyaan->tidak_tahu }}" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('sedikit_setuju'))
                        <small class="form-text text-danger">{{ $errors->first('sedikit_setuju') }}</small>
                    @endif
                    Sedikit Setuju
                    <input type="TEXT" class="form-control" id="sedikit_setuju" name="sedikit_setuju" placeholder="Enter nilai" value="{{ $pertanyaan->sedikit_setuju }}" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('cukup_setuju'))
                        <small class="form-text text-danger">{{ $errors->first('cukup_setuju') }}</small>
                    @endif
                    Cukup Setuju
                    <input type="TEXT" class="form-control" id="cukup_setuju" name="cukup_setuju" placeholder="Enter nilai" value="{{ $pertanyaan->cukup_setuju }}" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('setuju'))
                        <small class="form-text text-danger">{{ $errors->first('setuju') }}</small>
                    @endif
                    Setuju
                    <input type="TEXT" class="form-control" id="setuju" name="setuju" placeholder="Enter nilai" value="{{ $pertanyaan->setuju }}" step="any">
                  </div>
                  <div class="col-2">
                  @if ($errors->has('sangat_setuju'))
                        <small class="form-text text-danger">{{ $errors->first('sangat_setuju') }}</small>
                    @endif
                    Sangat Setuju
                    <input type="TEXT" class="form-control" id="sangat_setuju" name="sangat_setuju" placeholder="Enter nilai" value="{{ $pertanyaan->sangat_setuju }}" step="any">
                  </div>  
                </div>
                </div>
                <div class="form-group">
                <label for="nilai_bobot" class="form-label">
    Nilai CFH
    <span class="text-muted">(Gunakan titik untuk angka desimal, contoh : 0.60)</span>
</label>
                    @if ($errors->has('CFH'))
                        <small class="form-text text-danger">{{ $errors->first('CFH') }}</small>
                    @endif
                    <div class="row">
                    <div class="col-2">
                    <input type="TEXT" class="form-control" id="CFH" name="CFH" placeholder="Enter nilai" value="{{ $pertanyaan->CFH }}" step="any"> 
                </div>
                </div>
                </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
  <button type="submit" class="btn btn-primary float-right">SUBMIT</button>
</div>
                </div>
                </div>
                </div>
              </form>
        </form>
      </div><!-- /.container-fluid -->
    </section>
  </div>

<!-- jQuery -->
<script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if($message = Session::get('success'))
<script> 
Swal.fire({
  position: "center-center",
  icon: "success",
  title: "{{ $message }}",
  showConfirmButton: false,
  timer: 2500
});

 // JavaScript to disable the gender select based on a condition
 document.addEventListener('DOMContentLoaded', function() {
        // Example condition: disable if faktor is 'Kesiapan Emosi'
        var faktorElement = document.getElementById('faktor');
        var genderElement = document.getElementById('gender');

        faktorElement.addEventListener('change', function() {
            if (faktorElement.value === 'Kesiapan Emosi') {
                genderElement.disabled = true;
            } else {
                genderElement.disabled = false;
            }
        });

        // Initial check in case the form is pre-filled
        if (faktorElement.value === 'Kesiapan Emosi') {
            genderElement.disabled = true;
        }
    });
</script>                                  
@endif                              
@endsection