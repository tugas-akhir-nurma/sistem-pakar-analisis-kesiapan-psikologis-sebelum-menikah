@extends('layout.main')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.css" />
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-12">
            <h3 class="title">PERTANYAAN PSIKOLOGIS</h3>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data User</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <style>
  
    
    .content-wrapper {
        background-color: #dfe6da; /* Warna pink soft */ /* Ubah kode warna sesuai preferensi Anda */
    }
    .top-right-button {
      background-color: #343a40; /* Abu-abu tua */
            border-color: #343a40; /* Abu-abu tua */
            color: white;
            margin-top: 5px; /* Adjust this value as needed */
        }
        .btn-dark-gray {
            background-color: #343a40; /* Abu-abu tua */
            border-color: #343a40; /* Abu-abu tua */
            color: white;
            font-family: 'Times New Roman', Times, serif;
        }
        .title {
            font-family: 'Times New Roman', Times, serif; /* Mengubah font agar lebih formal untuk judul cetak */
            color: black; /* Warna teks menjadi hitam */
            margin-bottom: 0px; /* Jarak bawah antara judul */
            margin-top: 0px; /* Mengurangi margin atas untuk mendekatkan dengan logo */
            text-align: center; /* Menempatkan teks di tengah */
            font-weight: bold; /* Membuat teks tebal */
        }

        </style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
          <div class="col-12">
          <div class="container-fluid relative-container">
            <a href="{{ route('admin.createkesiapanpsikologis') }}" class="btn btn-dark-gray top-right-button mb-3">TAMBAH PERTANYAAN</a>
            </DIV>
            <div class="card">
              <div class="card-header">

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search"> -->

                    <div class="input-group-append">
                      
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <style>
    table {
    width: 100%; /* Sesuaikan lebar tabel sesuai kebutuhan */
    text-align: left;
    }
    .question-column {
      white-space: pre-line;
      word-wrap: break-word;
      max-width: 130px; 
      text-align: left;
    }
    .faktor-column {
      white-space: pre-line;
      word-wrap: break-word;
      max-width: 100px; 
      text-align: left;
    }
   
    .card{
      background-color: #eff5ec;
      font-family: 'Times New Roman', Times, serif; 
    margin-right: 30px; /* Adds left and right margins */
    margin-left:15px; 
    text-align: center;
    }
    #serverside th,
#serverside td {
    text-align: center;
    vertical-align: middle; /* Ensures text is vertically centered as well */
    background-color: #eff5ec;
}

/* Optional: Add some styling to the table */
#serverside {
    width: 100%; /* Adjust this value to set the table width as needed */
    border-collapse: collapse; /* Ensures there is no space between table cells */
    text-align: center;
}

#serverside th,
#serverside td {
    padding: 10px; /* Adds some padding for better readability */
    border: 1px solid #ddd; /* Adds a border to the cells */
    text-align: center;
}

#serverside th {
    background-color: #eff5ec; /* Adds a background color to the header */
}

    </style>
  <!-- <title>Your Page Title</title> -->
</head>
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap" id="pertanyaan">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Faktor</th>
                      <th>Kode</th>
                      <th>Pertanyaan</th>
                      <th>Bobot Jawaban</th>
                      <th>CFH</th>
                      <th>Gender</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($pertanyaan as $p)
                        <tr>
                            <td class="faktor-column"> {{ $loop->iteration }}</td>
                            <td class="faktor-column">{{ $p->faktor }}</td>
                            <td class="faktor-column">{{ $p->kode_kesiapan }}</td>
                            <td class="question-column">{{ $p->kesiapan }}</td>
                            <td>
                            <table class="table table-hover">
                              <tbody>
                                  <tr data-widget="expandable-table" aria-expanded="false">
                                      <td>
                                          <button type="button" class="btn btn-primary p-0">
                                              <i class="expandable-table-caret fas fa-caret-right fa-fw"></i>
                                          </button>
                                          Nilai
                                      </td>
                                  </tr>
                                  <tr class="expandable-body">
                                      <td>
                                          <div class="p-0">
                                              <table>
                                                  <tbody class="faktor-column">
                                                      <tr>
                                                          <td>Tidak={{ $p->tidak }}</td>
                                                      </tr>
                                                      <tr>
                                                          <td>Tidak Tahu={{ $p->tidak_tahu }}</td>
                                                      </tr>
                                                      <tr>
                                                          <td>Sedikit Setuju={{ $p->sedikit_setuju }}</td>
                                                      </tr>
                                                      <tr>
                                                          <td>Cukup Setuju={{ $p->cukup_setuju }}</td>
                                                      </tr>
                                                      <tr>
                                                          <td>Setuju={{ $p->setuju }}</td>
                                                      </tr>
                                                      <tr>
                                                          <td>Sangat Setuju={{ $p->sangat_setuju }}</td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                            </td>
                            <td class="faktor-column">{{ $p->CFH }}</td>
                            <td class="faktor-column">{{ $p->gender }}</td>
                            <td>
                                <a href="{{ route('admin.editkesiapanpsikologis',['id' => $p->id]) }}" class="btn btn-primary"><i class="fas fa-pen"></i>Edit</a>
                            </td>
                        </tr>
                        <div class="modal fade" id="modal-hapus{{ $p->id }}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi Hapus Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Apakah kamu yakin ingin menghapus data user <b>{{ $p->kode_kesiapan }}</b></p>
            </div>
                <form action="{{ route('admin.deletekesiapanpsikologis',['id' => $p->id]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-primary">Ya, Hapus</button>
                </div>
                </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
                    @endforeach    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if($message = Session::get('success'))
<script> 
Swal.fire({
  position: "center-center",
  icon: "success",
  title: "{{ $message }}",
  showConfirmButton: false,
  timer: 2500
});
</script>                                  
@endif 
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
<script>
$(document).ready( function () {
    $('#pertanyaan').DataTable();
} );
</script>
@endsection