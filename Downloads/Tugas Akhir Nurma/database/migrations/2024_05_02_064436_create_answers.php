<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap')->nullable(); // Ubah agar kolom dapat memiliki nilai NULL

            // Menambahkan kolom untuk menyimpan semua jawaban dalam format JSON
            $table->json('all_answers');

            $table->timestamps();

            // Menambahkan kunci asing ke kolom nama_lengkap yang mengacu pada kolom nama_lengkap di tabel jawabans
            $table->foreign('nama_lengkap')->references('nama_lengkap')->on('jawabans')
                  ->onDelete('cascade'); // Atur aksi saat menghapus data di tabel jawabans
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('answers');
    }
};
