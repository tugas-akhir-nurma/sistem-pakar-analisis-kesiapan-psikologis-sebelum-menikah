<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kesiapan_psikologis', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('kode_kesiapan', 4)->unique();
            $table->string('kesiapan');
            $table->string('faktor');
            $table->string('tidak');
            $table->string('tidak_tahu');
            $table->string('sedikit_setuju');
            $table->string('cukup_setuju');
            $table->string('setuju');
            $table->string('sangat_setuju');
            $table->string('CFH');
            $table->string('gender');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kesiapan_psikologis');
    }
};
