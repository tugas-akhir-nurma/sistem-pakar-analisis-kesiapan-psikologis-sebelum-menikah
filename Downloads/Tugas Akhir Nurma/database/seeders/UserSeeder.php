<?php

namespace Database\Seeders;

use App\Models\Pakar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash; 

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Pakar::create([
            'email'     => 'pakar123@gmail.com',
            'password'  => Hash::make('pakar123'),
        ]);
    }
}
