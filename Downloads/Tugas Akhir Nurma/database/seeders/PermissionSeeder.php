<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\Pakar;

class PermissionSeeder extends Seeder
{
    public function run(): void
    {
        $role_pakar = Role::firstOrCreate(['name' => 'pakar']);
        $role_pengantin = Role::firstOrCreate(['name' => 'pengantin']);
            
                // Mendapatkan semua pakar dan menetapkan peran 'pakar' kepada mereka
                $pakars = Pakar::all();
                foreach ($pakars as $pakar) {
                    $pakar->assignRole($role_pakar);
                }
            
                // Mendapatkan semua pengguna dan menetapkan peran 'pengantin' kepada mereka
                $pengantin_users = User::all();
                foreach ($pengantin_users as $pengantin) {
                    $pengantin->assignRole($role_pengantin);
                }

            }
            
            }