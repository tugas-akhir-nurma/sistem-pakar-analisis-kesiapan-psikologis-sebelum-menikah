<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_lengkap',
        'jenis_kelamin',
        'tanggal_analisis',
        'user_id',
        'nilai_akhir',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
