<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

        use HasRoles;
    
        protected $fillable = [
            'email', 'password','nama_lengkap','jenis_kelamin'
        ];
    
        protected $hidden = [
            'password', 'remember_token',
        ];
    
        protected $guard_name = 'web';

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ]; 

    public function jawabanusers()
    {
        return $this->hasMany(JawabanUser::class, 'user_id');
    }

    public function jawabans()
    {
        return $this->hasMany(Jawaban::class, 'user_id');
    }
}
