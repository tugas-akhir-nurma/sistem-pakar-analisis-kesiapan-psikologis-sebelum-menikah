<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalHasilFaktor extends Model
{
    use HasFactory;
    protected $table = "total_hasil_faktors1";
    protected $fillable = [
        'faktor',
        'total_cf',
        'riwayat_user_id',
    ];
    
}
