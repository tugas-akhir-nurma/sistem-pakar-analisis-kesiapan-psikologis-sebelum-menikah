<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KesiapanPsikologis extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->kode_kesiapan = 'P' . str_pad((static::count() + 1), 3, '0', STR_PAD_LEFT);
        });
    
        static::deleting(function ($model) {
            $model->updateLowerCodes();
        });
    }

//     public function updateLowerCodes()
// {
//     $lowerCodes = self::where('id', '>', $this->id)->get();

//     foreach ($lowerCodes as $index => $lowerCode) {
//         $lowerCode->kode_kesiapan = 'P' . str_pad($index - 1, 3, '0', STR_PAD_LEFT);
//         $lowerCode->save();
//     }
//     }

    use HasFactory;
    protected $fillable = [
        'kesiapan',
        'gender',
        'faktor',
        'tidak',
        'tidak_tahu',
        'sedikit_setuju',
        'cukup_setuju',
        'setuju',
        'sangat_setuju',
        'CFH',
        'pakar_id',
    ];

    public function faktor()
    {
        return $this->belongsTo(Faktor::class);
    }

    public function pakars()
    {
        return $this->belongsTo(Pakar::class, 'pakar_id');
    }

    public function jawabanusers()
    {
        return $this->hasMany(JawabanUser::class, 'kesiapan_psikologis_id');
    }
}
