<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JawabanUser extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'kesiapan_psikologis_id',
        'CFE',
        'CFH',
        'CF',
        'jawaban',
        'riwayat_user_id',
        'CFCombine',
    ];

    public function kesiapanPsikologis()
    {
        return $this->belongsTo(KesiapanPsikologis::class, 'kesiapan_psikologis_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
