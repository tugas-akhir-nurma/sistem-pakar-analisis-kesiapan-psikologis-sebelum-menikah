<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
// use App\Models\Pakar;
use App\Http\Middleware\RedirectIfAuthenticated;

class LoginController extends Controller
{
    //

    public function index(){
        return view('auth.login');
    }

    public function login_proses(Request $request)
{
    $request->validate([
        'email' => 'required',
        'password' => 'required'
    ]);
    
    $data = [
        'email'     => $request->email,
        'password'  => $request->password
    ];

    if (Auth::attempt($data)) {
        return redirect()->route('user.dashboarduser');
    } else {
        return redirect()->route('login')->with('failed', 'Email atau Password Salah');
    }
}

    public function logout(){
        Auth::logout();
        return redirect()->route('login')->with('success','Kamu berhasil logout');
    }

    public function register(){
        return view('auth.register');
    }

    public function register_proses(Request $request)
    {
        // Validasi input
        $request->validate([
            'email' => 'required|email|unique:users,email|unique:pakars,email',
            'password' => 'required|min:6',
            'nama_lengkap' => 'required',
            'jenis_kelamin' => 'required',
        ]);
    
        // Data untuk dimasukkan ke dalam tabel pengguna
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $data['nama_lengkap'] = $request->nama_lengkap;
        $data['jenis_kelamin'] = $request->jenis_kelamin;
    
        // Mencoba membuat pengguna
        $user = User::create($data);
    
        // Jika berhasil membuat pengguna
        if ($user) {
            // Mencoba melakukan login
            $login = [
                'email'     => $request->email,
                'password'  => $request->password,
            ];
    
            if (Auth::attempt($login)) {
                return redirect()->route('login');
            } else {
                return redirect()->route('login')->with('failed', 'Email atau Password Salah');
            }
        } else {
            return redirect()->route('register')->with('failed', 'Gagal membuat pengguna');
        }
    }
    

    
}