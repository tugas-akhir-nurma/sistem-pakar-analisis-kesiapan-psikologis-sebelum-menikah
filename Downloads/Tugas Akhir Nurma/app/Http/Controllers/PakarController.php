<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pakar;
use App\Models\jawaban;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Middleware\RedirectIfAuthenticated;

class PakarController extends Controller
{
//     public function __construct()
//     {
//         $this->middleware('auth:pakar');
//     }
    
//     public function index()
//     {
//         // Gunakan guard 'admin'
//         if (Auth::guard('pakar')->check()) {
//             // Pengguna admin telah terotentikasi
//         }
//     }
    
    public function indexpakar(){
        $pakar = Pakar::get();
       return view('pakar.indexpakar',compact('pakar'));
    } 

    public function createpakar(){
         return view('pakar.createpakar');
    }

    public function storepakar(Request $request){

         $validator = Validator::make($request->all(),[
              'email' => 'required|email',
              'password' => 'required',
         ]);

         if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

         $pakar['email']      = $request->email;
         $pakar['password']   = Hash::make($request->password);

         Pakar::create($pakar);

         return redirect()->route('admin.indexpakar')->with('success','Data berhasil ditambahkan');
         }

    public function editpakar(Request $request,$id){
         $pakar = Pakar::find($id);

         return view('pakar.editpakar',compact('pakar'));
    }

    public function updatepakar(Request $request,$id){
         $validator = Validator::make($request->all(),[
              'email'    => 'required|email',
              'password' => 'nullable',
         ]);

         if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

         $pakar['email']      = $request->email;
         if($request->password){
         $pakar['password']   = Hash::make($request->password);
         }

         Pakar::whereId($id)->update($pakar);

         return redirect()->route('admin.indexpakar')->with('success','Data berhasil diubah');
    }

    public function deletepakar(Request $request,$id){
         $pakar = Pakar::find($id);

         if($pakar)
              $pakar->delete();

         return redirect()->route('admin.indexpakar')->with('success','Data berhasil dihapus');
    }

    public function pakarlogin(){
     return view('pakarlogin');
 }

 public function pakarlogin_proses(Request $request)
{
    $request->validate([
        'email' => 'required|email',
        'password' => 'required'
    ]);

    // Ambil semua data pengguna dari tabel 'pakar'
    $pakars = Pakar::get();

    // Loop melalui setiap pengguna dan cek apakah ada yang cocok dengan email dan password yang diberikan
    foreach ($pakars as $pakar) {
        if ($pakar->email === $request->email && Hash::check($request->password, $pakar->password)) {
            return redirect()->route('admin.dashboard');
        }
    }

    // Jika tidak ada pengguna yang cocok, kembalikan ke halaman login dengan pesan kesalahan
    return redirect()->route('pakarlogin')->with('failed', 'Email atau Password Salah');
} 
}
