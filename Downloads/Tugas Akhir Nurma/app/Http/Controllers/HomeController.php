<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\kesiapanpsikologis;
use App\Models\jawaban;
use App\Models\Pakar;
use App\Models\Answer;
use App\Models\jawabanuser;
use App\Models\TotalHasilFaktor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Middleware\RedirectIfAuthenticated;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;
use App\helper\Rumus;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
     public function dashboard()
     {
      return view('dashboard');
     }
      
     public function index(){
         $data = User::get();
        return view('index',compact('data'));
     } 

     public function edituser(Request $request,$id){
          $data = User::find($id);

          return view('edituser',compact('data'));
     }

     public function updateuser(Request $request,$id){
          $validator = Validator::make($request->all(),[
               'email'    => 'required|email',
               'password' => 'nullable',
          ]);

          if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

          $data['email']      = $request->email;
          if($request->password){
          $data['password']   = Hash::make($request->password);
          }

          User::whereId($id)->update($data);

          return redirect()->route('admin.index')->with('success','Data berhasil diubah');
     }

     public function deleteuser(Request $request,$id){
          $data = User::find($id);

          if($data)
               $data->delete();

          return redirect()->route('admin.index')->with('success','Data berhasil dihapus');
     }

     public function kesiapanpsikologis(){
         $pertanyaan = kesiapanpsikologis::get();
        return view('kesiapan psikologis.index',compact('pertanyaan'));
     } 

     public function createkesiapanpsikologis(){
          return view('kesiapan psikologis.create');
     }

     public function storekesiapanpsikologis(Request $request){
         
          $validator = Validator::make($request->all(),[
               'faktor' => 'required',
               'gender' => 'required',
               'kesiapan' => 'required',
               'tidak' => 'required', 
               'tidak_tahu' => 'required',
               'sedikit_setuju' => 'required',
               'cukup_setuju' => 'required',
               'setuju' => 'required',
               'sangat_setuju' => 'required',
               'CFH' => 'required',
          ]);

          if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

          $pertanyaan['faktor']      = $request->faktor;
          $pertanyaan['gender']      = $request->gender;
          $pertanyaan['kode_kesiapan']   = $request->kode_kesiapan;
          $pertanyaan['kesiapan']   = $request->kesiapan;
          $pertanyaan['tidak']   = $request->tidak;
          $pertanyaan['tidak_tahu']   = $request->tidak_tahu;
          $pertanyaan['sedikit_setuju']   = $request->sedikit_setuju;
          $pertanyaan['cukup_setuju']   = $request->cukup_setuju;
          $pertanyaan['setuju']   = $request->setuju;
          $pertanyaan['sangat_setuju']   = $request->sangat_setuju;
          $pertanyaan['CFH']   = $request->CFH;
          

          kesiapanpsikologis::create($pertanyaan);

          return redirect()->route('admin.kesiapanpsikologis')->with('success','Data berhasil ditambahkan');
          }

          public function editkesiapanpsikologis(Request $request,$id){
               $pertanyaan = kesiapanpsikologis::find($id);
     
               return view('kesiapan psikologis.edit',compact('pertanyaan'));
          }
     
          public function updatekesiapanpsikologis(Request $request,$id){
               $validator = Validator::make($request->all(),[
                    'faktor' => 'required',
                    'gender' => 'required',
                    'kesiapan' => 'required',
                    'tidak' => 'required', 
                    'tidak_tahu' => 'required',
                    'sedikit_setuju' => 'required',
                    'cukup_setuju' => 'required',
                    'setuju' => 'required',
                    'sangat_setuju' => 'required',
                    'CFH' => 'required',
                    'gender' => 'required',
               ]);
     
               if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
     
               $pertanyaan['faktor']      = $request->faktor;
               $pertanyaan['gender']      = $request->gender;
               $pertanyaan['kesiapan']   = $request->kesiapan;
               $pertanyaan['tidak']   = $request->tidak;
               $pertanyaan['tidak_tahu']   = $request->tidak_tahu;
               $pertanyaan['sedikit_setuju']   = $request->sedikit_setuju;
               $pertanyaan['cukup_setuju']   = $request->cukup_setuju;
               $pertanyaan['setuju']   = $request->setuju;
               $pertanyaan['sangat_setuju']   = $request->sangat_setuju;
               $pertanyaan['CFH']   = $request->CFH;
               $pertanyaan['gender']      = $request->gender;
     
               kesiapanpsikologis::whereId($id)->update($pertanyaan);
     
               return redirect()->route('admin.kesiapanpsikologis')->with('success','Data berhasil diubah');
          }

          public function dashboarduser()
          {
              // Ambil data riwayat analisis hanya untuk user yang sedang login
              $userId = Auth::id();
              $analisis = Jawaban::where('user_id', $userId)->get();
              $results = TotalHasilFaktor::where('user_id', auth()->user()->id)->get();
      
              // Kirim data ke tampilan
              return view('user.dashboarduser', compact('analisis','results'));
          }
         
          public function indexanalisis(){
            $data = User::find (auth()->id());
            $namalengkap =$data->nama_lengkap;
            $jeniskelamin =$data->jenis_kelamin;
            return view('user.indexanalisis',compact('jeniskelamin','namalengkap'));
          }
      
          public function storeanalisis(Request $request)
{
//     $validator = Validator::make($request->all(), [
//         'nama_lengkap' => 'required',
//         'jenis_kelamin' => 'required',
//     ]);

//     if ($validator->fails()) {
//         return redirect()->back()->withInput()->withErrors($validator);
//     }

//     $tanggal_analisis = Carbon::now()->toDateString();
// //     $email = Auth::user()->email;

//     $analisis['nama_lengkap'] = $request->nama_lengkap;
//     $analisis['jenis_kelamin'] = $request->jenis_kelamin;
//     $analisis['tanggal_analisis'] = $tanggal_analisis;
//   
// jawaban::create($analisis);

    // Cek jenis kelamin dan arahkan ke rute yang sesuai
    $data = User::find (auth()->id());
    $namalengkap =$data->nama_lengkap;
    $jeniskelamin =$data->jenis_kelamin;
    if ($jeniskelamin == 'Laki-Laki') {
        // jawaban::create($analisis);
        return redirect()->route('user.analisispsikologis1')->with('namalengkap', $namalengkap);;
    } elseif ($jeniskelamin == 'Perempuan') {
        // jawaban::create($analisis);
        return redirect()->route('user.analisispsikologis2')->with('namalengkap', $namalengkap);;
    }

    // Jika jenis kelamin tidak valid, arahkan kembali dengan pesan error
    return redirect()->back()->withInput()->with('error', 'Jenis kelamin tidak valid');
}

              public function deleteanalisis(Request $request,$id){
               $analisis = jawaban::find($id);
     
               if($analisis)
                    $analisis->delete();
     
               return redirect()->route('admin.jawabanuser')->with('success','Data berhasil dihapus');
          }
      
              public function jawabanuser(){
                 $analisis = jawaban::get();
               return view('jawabanuser',compact('analisis'));
          }
              
      
          public function analisispsikologis1()
          {
           $pertanyaan = kesiapanpsikologis::get();
           return view('user.analisispsikologis1',compact('pertanyaan'));
          }


public function storepsikologis(Request $request)
{
    $allAnswers = $request->except('_token');

    $data = User::find(auth()->id());
    $namalengkap = $data->nama_lengkap;
    $jeniskelamin = $data->jenis_kelamin;
    $tanggal_analisis = Carbon::now()->toDateString();
    $jawaban = new jawaban;
    $jawaban -> user_id = auth()->id();
    $jawaban -> tanggal_analisis = $tanggal_analisis;
    $jawaban -> nama_lengkap = $namalengkap; // add this line
    $jawaban -> jenis_kelamin = $jeniskelamin; // add this line
    $jawaban -> save();
    $id = $jawaban->id;

    $CFSebelum = 0;

    foreach ($allAnswers as $key => $jawab) {
        $jawabanuser = explode('|', $jawab);
        $cf = (float) $jawabanuser[1] * (float) $jawabanuser[0];
        if ($CFSebelum == 0 ){
            $Combine = 0;
            $CFSebelum = $cf;
        }else {
            $Combine = Rumus::CFCombine($CFSebelum, $cf);
            $CFSebelum = $Combine;
        }

        // Menyimpan data ke dalam database
        jawabanuser::create([
            'user_id' => auth()->id(),
            'kesiapan_psikologis_id' => $key,
            'CFE' => (float) $jawabanuser[0],
            'CFH' => (float) $jawabanuser[1],
            'jawaban' =>  $jawabanuser[2],
            'CF' => $cf,
            'CFCombine' => $Combine,
            'riwayat_user_id' => $id,
        ]);
    }
    $hasil = number_format($Combine * 100, 2);
    $jawaban -> nilai_akhir = $hasil;
    $jawaban -> update();
    $nama_lengkap = User::find(auth()->id())->nama_lengkap;
    $jenis_kelamin = User::find(auth()->id())->jenis_kelamin;

    // Mendapatkan riwayat_user_id dari user yang sedang login
    $riwayatUserId = $id; // Pastikan $id adalah riwayat_user_id yang benar

    // Mendapatkan faktor dan total_cf berdasarkan riwayat_user_id
    $totalHasilFaktor = TotalHasilFaktor::where('riwayat_user_id', $riwayatUserId)->get();
    $faktor = $totalHasilFaktor->pluck('faktor');
    $total_cf = $totalHasilFaktor->pluck('total_cf');

    // Menyiapkan data untuk view
    $results = $totalHasilFaktor->map(function ($item) {
    return [
        'faktor' => $item->faktor,
        'total_cf' => number_format($item->total_cf , 2), // Memformat total_cf
        'cfmaksimal' => $item->cfmaksimal,
    ];
});

    return view('user.hasilanalisis', [ 'results' => $results, 'hasil' => $hasil, 'nama_lengkap' => $nama_lengkap, 'jenis_kelamin' => $jenis_kelamin, 'tanggal_analisis' => $tanggal_analisis]);
      

    // Answer::create([
    //     'nama_lengkap' => $namaLengkap,
    //     'all_answers' => $allAnswers,
    // ]);
    }
    // return redirect()->route('user.dashboarduser');

          public function analisispsikologis2()
          {
           $pertanyaan = kesiapanpsikologis::get();
           return view('user.analisispsikologis2',compact('pertanyaan'));
          }

          public function infopsikologis()
          {
            return view('user.infopsikologis');
          }

          public function infopranikah()
          {
            return view('user.infopranikah');
          }

          public function pakarlogin(){
            return view('pakarlogin');
        }
       
       public function pakarlogin_proses(Request $request)
{
    $request->validate([
        'email' => 'required|email',
        'password' => 'required'
    ]);

    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials)) {
        // Authentication passed...
        return redirect()->route('admin.dashboard');
    }

    // If authentication fails, redirect back to the login with an error message
    return redirect()->route('pakarlogin')->with('failed', 'Email atau Password Salah');
}

public function getDataPrint($id){
    

    $jawab = Jawaban::find($id);

    return response()->json($jawab);
    
}

  
}