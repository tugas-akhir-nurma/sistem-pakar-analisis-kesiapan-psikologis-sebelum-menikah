<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            // Jika pengguna sudah terotentikasi, redirect ke dashboard yang sesuai
            if ($guard === 'pengantin') {
                return redirect('/login');
            }
            // } elseif ($guard === 'user') {
            //     return redirect()->route('user.dashboarduser');
            // }
        }

        return $next($request);
    }
}
