<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PakarController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Controller;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\helper\Rumus;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     echo "Hello World"; 
// });

Route::get('/login',[LoginController::class,'index'])->name('login')->middleware('guest');
Route::get('/loginadmin',[PakarController::class,'pakarlogin'])->name('pakarlogin');
Route::post('/login-proses',[LoginController::class,'login_proses'])->name('login-proses');
Route::post('/loginadmin-proses',[PakarController::class,'pakarlogin_proses'])->name('pakarlogin-proses');
Route::get('/logout',[LoginController::class,'logout'])->name('logout');
Route::get('/register',[LoginController::class,'register'])->name('register');
Route::post('/register-proses',[LoginController::class,'register_proses'])->name('register-proses');

Route::group(['prefix' => 'admin', 'as' => 'admin.'] , function(){
Route::get('/dashboard',[HomeController::class,'dashboard'])->name('dashboard');

Route::get('/user',[HomeController::class,'index'])->name('index');

Route::get('/edituser/{id}',[HomeController::class,'edituser'])->name('edituser');
Route::put('/updateuser/{id}',[HomeController::class,'updateuser'])->name('updateuser');
Route::delete('/deleteuser/{id}',[HomeController::class,'deleteuser'])->name('deleteuser');

Route::get('/pakar',[PakarController::class,'indexpakar'])->name('indexpakar');
Route::get('/createpakar',[PakarController::class,'createpakar'])->name('createpakar');
Route::post('/storepakar',[PakarController::class,'storepakar'])->name('storepakar');

Route::get('/editpakar/{id}',[PakarController::class,'editpakar'])->name('editpakar');
Route::put('/updatepakar/{id}',[PakarController::class,'updatepakar'])->name('updatepakar');
Route::delete('/deletepakar/{id}',[PakarController::class,'deletepakar'])->name('deletepakar');

Route::get('/kesiapanpsikologis',[HomeController::class,'kesiapanpsikologis'])->name('kesiapanpsikologis');
Route::get('/createkesiapanpsikologis',[HomeController::class,'createkesiapanpsikologis'])->name('createkesiapanpsikologis');
Route::post('/storekesiapanpsikologis',[HomeController::class,'storekesiapanpsikologis'])->name('storekesiapanpsikologis');

Route::get('/editkesiapanpsikologis/{id}',[HomeController::class,'editkesiapanpsikologis'])->name('editkesiapanpsikologis');
Route::put('/updatekesiapanpsikologis/{id}',[HomeController::class,'updatekesiapanpsikologis'])->name('updatekesiapanpsikologis');
Route::delete('/deletekesiapanpsikologis/{id}',[HomeController::class,'deletekesiapanpsikologis'])->name('deletekesiapanpsikologis');


Route::get('/jawabanuser',[HomeController::class,'jawabanuser'])->name('jawabanuser');

Route::get('/print-data/{id}',[HomeController::class,'getDataPrint']);

});

Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
Route::get('/dashboarduser',[HomeController::class,'dashboarduser'])->name('dashboarduser');

Route::get('/analisis',[HomeController::class,'indexanalisis'])->name('indexanalisis');
Route::post('/storeanalisis',[HomeController::class,'storeanalisis'])->name('storeanalisis');
Route::delete('/deleteanalisis/{id}',[HomeController::class,'deleteanalisis'])->name('deleteanalisis');
Route::get('/analisispsikologis1',[HomeController::class,'analisispsikologis1'])->name('analisispsikologis1');
Route::post('/storepsikologis',[HomeController::class,'storepsikologis'])->name('storepsikologis');
Route::get('/analisispsikologis2',[HomeController::class,'analisispsikologis2'])->name('analisispsikologis2');
Route::get('/hasilanalisis/{id}',[HomeController::class,'hasilanalisis'])->name('hasilanalisis');
Route::get('/psikologispernikahan',[HomeController::class,'infopsikologis'])->name('infopsikologis');
Route::get('/infopranikah',[HomeController::class,'infopranikah'])->name('infopranikah');

});

Route::get('/rumus-user-helper-demo', function () {
    return Rumus::CFCombine(2, 5);
});
